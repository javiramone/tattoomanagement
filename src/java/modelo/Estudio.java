package modelo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Estudio implements Serializable {

    private int id;
    private String nombre;
    private String dir;
    private String mail;
    private int nTelefono;
    private final HashMap artistasAsociados;

    public Estudio(int id, String nombre, String dir, String mail, int nTelefono) {
        artistasAsociados = new HashMap();
        this.id = id;
        this.nombre = nombre;
        this.dir = dir;
        this.mail = mail;
        this.nTelefono = nTelefono;
    }

    public Map getArtistas() {
        return artistasAsociados;
    }

    public String getMail() {
        return mail;
    }

    public String getNombre() {
        return nombre;
    }

    public int getnTelefono() {
        return nTelefono;
    }

    public String getDir() {
        return this.dir;
    }

    public int getID() {
        return this.id;
    }

}
