package modelo;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;

public class Cita implements Serializable {

    private String codigo;
    private String fecha;
    private double duracion;
    private double precio;
    private int telefonoContacto;
    private String nombreContacto;
    private int fianza;
    private int recordatorio;
    private String detalles;
    private String idUsuario;

    public Cita(String codigo, String fecha, double duracion, double precio, int telefonoContacto,
            String nombreContacto, int fianza, int recordatorio, String detalles, String idUsuario) {
        this.codigo = codigo;
        this.fecha = fecha;
        this.duracion = duracion;
        this.precio = precio;
        this.telefonoContacto = telefonoContacto;
        this.nombreContacto = nombreContacto;
        this.fianza = fianza;
        this.recordatorio = recordatorio;
        this.detalles = detalles;
        this.idUsuario = idUsuario;
    }

    public String getCodigo() {
        return codigo;
    }

    public Timestamp getFecha() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        java.util.Date utilDate = null;
        Timestamp sqlDate = null;
        try {
            utilDate = formatter.parse(fecha);
            sqlDate = new Timestamp(utilDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sqlDate;
    }

    public Date getFechaSQL() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        java.util.Date utilDate = null;
        Date sqlDate = null;
        try {
            utilDate = formatter.parse(fecha);
            sqlDate = new Date(utilDate.getTime());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return sqlDate;
    }

    public java.util.Date getFechaDate() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        java.util.Date utilDate = null;
        Date sqlDate = null;
        try {
            utilDate = formatter.parse(this.fecha);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return utilDate;
    }

    public String getFechaString() {
        String fechaNueva = fecha.substring(0, 10);
        String replace = fechaNueva.replace('-', '/');
        return replace;
    }

    public String getHoraString() {
        return fecha.substring(11, 16);
    }

    public String getOnlyHoraString() {
        return fecha.substring(11, 13);
    }

    public String getFechaHoraString() {
        String fecha = this.getFechaString();
        String hora = this.getHoraString();
        String result = fecha + " " + hora;
        return result;
    }

    public double getDuracion() {
        return duracion;
    }

    public int getDuracionInt() {
        return (int) duracion;
    }

    public double getPrecio() {
        return precio;
    }

    public int getPrecioInt() {
        return (int) precio;
    }

    public int getTelefonoContacto() {
        return telefonoContacto;
    }

    public String getNombreContacto() {
        return nombreContacto;
    }

    public int getFianza() {
        return fianza;
    }

    public String getFianzaString() {
        if (fianza == 1) {
            return "Si";
        } else {
            return "No";
        }
    }

    public int getRecordatorio() {
        return recordatorio;
    }

    public String getRecordatorioString() {
        if (recordatorio == 1) {
            return "Si";
        } else {
            return "No";
        }
    }

    public String getDetalles() {
        return detalles;
    }

    public String getIdUsuario() {
        return idUsuario;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public void setDuracion(double duracion) {
        this.duracion = duracion;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    public void setTelefonoContacto(int telefonoContacto) {
        this.telefonoContacto = telefonoContacto;
    }

    public void setNombreContacto(String nombreContacto) {
        this.nombreContacto = nombreContacto;
    }

    public void setFianza(int fianza) {
        this.fianza = fianza;
    }

    public void setDetalles(String detalles) {
        this.detalles = detalles;
    }
}
