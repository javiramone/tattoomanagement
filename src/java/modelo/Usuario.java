package modelo;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Usuario
        implements Serializable {

    private String nombre;
    private String estudio;
    private String mail;
    private String pass;
    private int viaCom;
    private Map citasAsociadas;
    private Map fechasAsociadas;
    private int nTelefono;
    private int idEstudio;

    public Usuario() {
        citasAsociadas = new HashMap();
        fechasAsociadas = new HashMap();
    }

    public Usuario(String nombre, String mail, String pass, int nTelefono, int viaCom, int idEstudio) {
        citasAsociadas = new HashMap();
        fechasAsociadas = new HashMap();
        this.nombre = nombre;
        this.mail = mail;
        this.pass = pass;
        this.nTelefono = nTelefono;
        this.viaCom = viaCom;
        this.idEstudio = idEstudio;
    }

    public Map getCitas() {
        return citasAsociadas;
    }

    public int getCantidadCitas() {
        return citasAsociadas.size();
    }

    public Map getFechas() {
        return fechasAsociadas;
    }

    public int getEstudio() {
        return idEstudio;
    }

    public String getMail() {
        return mail;
    }

    public String getNombre() {
        return nombre;
    }

    public String getPass() {
        return pass;
    }

    public int getIdEStudio() {
        return idEstudio;
    }

    public int getnTelefono() {
        return nTelefono;
    }

    public int getViaCom() {
        return viaCom;
    }

    public void setEstudio(String estudio) {
        this.estudio = estudio;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public void setnTelefono(int tlf) {
        nTelefono = tlf;
    }

    public void setviaCom(int viaCom) {
        this.viaCom = viaCom;
    }
}
