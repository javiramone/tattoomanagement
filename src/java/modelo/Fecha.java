package modelo;

import java.io.Serializable;

public class Fecha
        implements Serializable {

    private String fecha;
    private String fechaStr;
    private int horasLibres;
    private int festivo;

    public Fecha(String fecha, String fechaStr, int horasLibres, int festivo) {
        this.fecha = fecha;
        this.fechaStr = fechaStr;
        this.horasLibres = horasLibres;
        this.festivo = festivo;
    }

    public String getFecha() {
        return fecha;
    }

    public String getFechaStr() {
        return fechaStr;
    }

    public int getHorasLibres() {
        return horasLibres;
    }

    public int getFestivo() {
        return festivo;
    }

    public void setHorasLibres(int horas) {
        horasLibres = horas;
    }
}
