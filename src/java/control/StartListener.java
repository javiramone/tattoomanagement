package control;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Authenticator;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.owasp.esapi.errors.ValidationException;
import persistencia.PersistenceFactory;
import persistencia.PersistenceInterface;

public class StartListener
        implements ServletContextListener {

    private PersistenceInterface persistence;

    public void contextInitialized(ServletContextEvent sce) {
        if (!startValidate(sce.getServletContext())) {
            throw new RuntimeException("No se ha podido iniar la aplicación, parámetros de contexto incorrectos");
        }
        String persistenceMethod = sce.getServletContext().getInitParameter("persistenceMethod");
        String datos = sce.getServletContext().getInitParameter("archivoDatos");
        String historiales = sce.getServletContext().getInitParameter("archivoHistoriales");
        String log = sce.getServletContext().getInitParameter("archivoLog");
        String recover = sce.getServletContext().getInitParameter("archivoRecuperacion");
        this.persistence = PersistenceFactory.getInstance(persistenceMethod);
        //boolean exito = this.persistence.init(datos, historiales, log, recover);
        boolean exito = true;
        if (!exito) {
            throw new RuntimeException("Errores en la incialización de persistencia, imposible iniciar aplicación");
        }
        SendMail mail = new SendMail(sce.getServletContext());
        Authenticator autorizacionMail = mail.getAuth();
        sce.getServletContext().setAttribute("EmailSend", mail);
        sce.getServletContext().setAttribute("autorizacionMaile", autorizacionMail);
        sce.getServletContext().setAttribute("persistence", this.persistence);
    }

    public void contextDestroyed(ServletContextEvent sce) {
        boolean exito = this.persistence.exit();
        if (!exito) {
            Logger.getLogger(StartListener.class.getName()).log(Level.SEVERE, "Ha habido errores cerrando la persistencia (archivos)");
        }
    }

    private boolean startValidate(ServletContext context) {
        String persistencia = context.getInitParameter("persistenceMethod");
        String datos = context.getInitParameter("archivoDatos");
        String historiales = context.getInitParameter("archivoHistoriales");
        String recover = context.getInitParameter("archivoRecuperacion");
        String log = context.getInitParameter("archivoLog");
        String adminMail = context.getInitParameter("adminMail");
        String adminPass = context.getInitParameter("adminPass");
        String hostMail = context.getInitParameter("hostMail");
        String TSLmail = context.getInitParameter("TSLMail");
        String mailUser = context.getInitParameter("mailUser");
        String mailPort = context.getInitParameter("mailPort");
        String mailAuth = context.getInitParameter("authMail");
        String mailFrom = context.getInitParameter("mailFrom");
        String mailPass = context.getInitParameter("mailPass");
        if ((persistencia == null) || (adminMail == null) || (adminPass == null) || (hostMail == null) || (TSLmail == null) || (mailUser == null) || (mailPort == null) || (mailAuth == null) || (mailFrom == null) || (mailPass == null)) {
            return false;
        }
        if ((!persistencia.equals("file")) && (!persistencia.equals("pool"))) {
            return false;
        }
        if ((persistencia.equals("file") == true) && ((datos == null) || (historiales == null) || (recover == null) || (log == null))) {
            return false;
        }
        if ((persistencia.equals("pool") == true) && ((datos == null) || (historiales == null))) {
            return false;
        }
        try {
            Tools.validateEmail(adminMail);
            Tools.validatePass(adminPass);
            Tools.validateEmail(mailFrom);
        } catch (ValidationException ex) {
            Logger.getLogger(StartListener.class.getName()).log(Level.SEVERE, "Se ha detectado un posible problema de seguridad en los parámetros del descriptor de despliegue", ex);

            return false;
        }
        return true;
    }
}
