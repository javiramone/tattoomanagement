package control;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class LogOutServlet
        extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if ((request.getSession().getAttribute("auth") != null) && (((Boolean) request.getSession().getAttribute("auth")).booleanValue() == true)) {
            request.getSession().invalidate();
            if (request.getAttribute("errorSesion") != null) {
                request.getRequestDispatcher("/login.jsp").forward(request, response);
                return;
            }
            request.getRequestDispatcher("/logout.jsp").forward(request, response);
        } else {
            response.sendRedirect("/index.jsp");
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet encargado del cierre de sesión de usuarios";
    }
}
