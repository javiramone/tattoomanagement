package control.admin;

import control.Tools;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Usuario;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import persistencia.PersistenceInterface;

public class EditUserCompleteServlet
        extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (validateForm(request) == true) {
            try {
                PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
                String mail = Tools.validateEmail(request.getParameter("mail"));
                String nombre = Tools.validateName(request.getParameter("nombre"));
                String estudio = Tools.validateEstudio(request.getParameter("estudio"));
                String pass = Tools.validatePass(request.getParameter("pass"));
                String repeatPass = Tools.validatePass(request.getParameter("repeatPass"));
                int idEstudio = 1;
                int nTelefono;
                if (request.getParameter("nTelefono") == null) {
                    nTelefono = 0;
                } else {
                    nTelefono = Integer.parseInt(request.getParameter("nTelefono"));
                }
                String[] viasCom = request.getParameterValues("viaCom");
                int viaCom = 0;
                for (int x = 0; x < viasCom.length; x++) {
                    viaCom += Integer.parseInt(viasCom[x]);
                }
                Usuario userOld = persistencia.getUser((String) request.getSession().getAttribute("usuario"));
                if (userOld.getMail().equals(mail)) {
                    if (pass.equals(repeatPass)) {
                        String passMd = Tools.generateMD5Signature(pass + pass.toLowerCase());
                        Usuario userNew = new Usuario(nombre, mail, passMd, nTelefono, viaCom, idEstudio);
                        Usuario usuario = usuarioActualizar(userOld, userNew);
                        boolean okCita = persistencia.updateResponsableCita(userOld.getMail(), mail);
                        boolean ok = persistencia.updateUser(userOld.getMail(), usuario);
                        if ((ok) && (okCita) && (!userOld.getMail().equals(userNew.getMail()))) {
                            request.setAttribute("resultados", "Usuario actualizado");
                            Tools.anadirMensaje(request, "Su usuario ha sido actualizado correctamente");
                            request.getSession().invalidate();
                            request.getRequestDispatcher("/logout.jsp").forward(request, response);
                        } else if ((ok) && (okCita) && (userOld.getMail().equals(userNew.getMail()))) {
                            request.setAttribute("resultados", "Usuario actualizado");
                            Tools.anadirMensaje(request, "Su usuario ha sido actualizado correctamente");
                        } else {
                            request.setAttribute("resultados", "Error en la actualización del usuario");
                            Tools.anadirMensaje(request, "Ya hay un usuario registrado con este email");
                        }
                    } else {
                        request.setAttribute("resultados", "Datos incorrectos");
                        Tools.anadirMensaje(request, "La contraseña no coincide con su repeticion");
                    }
                } else if (persistencia.checkMail(mail)) {
                    if (pass.equals(repeatPass)) {
                        String passMd = Tools.generateMD5Signature(pass + pass.toLowerCase());
                        Usuario userNew = new Usuario(nombre, mail, passMd, nTelefono, viaCom, idEstudio);
                        Usuario usuario = usuarioActualizar(userOld, userNew);
                        boolean okCita = persistencia.updateResponsableCita(userOld.getMail(), mail);
                        boolean ok = persistencia.updateUser(userOld.getMail(), usuario);
                        if ((ok) && (okCita) && (!userOld.getMail().equals(userNew.getMail()))) {
                            request.setAttribute("resultados", "Usuario actualizado");
                            Tools.anadirMensaje(request, "Su usuario ha sido actualizado correctamente");
                            request.getSession().invalidate();
                            request.getRequestDispatcher("/logout.jsp").forward(request, response);
                        } else if ((ok) && (okCita) && (userOld.getMail().equals(userNew.getMail()))) {
                            request.setAttribute("resultados", "Usuario actualizado");
                            Tools.anadirMensaje(request, "Su usuario ha sido actualizado correctamente");
                        } else {
                            request.setAttribute("resultados", "Error en la actualización del usuario");
                            Tools.anadirMensaje(request, "Ya hay un usuario registrado con este email");
                        }
                    } else {
                        request.setAttribute("resultados", "Datos incorrectos");
                        Tools.anadirMensaje(request, "La contraseña no coincide con su repeticion");
                    }
                } else {
                    request.setAttribute("resultados", "Usuario no encontrado");
                    Tools.anadirMensaje(request, "Ya hay un usuario registrado con este email");
                }
            } catch (IntrusionException ex) {
                request.setAttribute("resultados", "Intrusión detectada");
                Tools.anadirMensaje(request, ex.getUserMessage());
            } catch (ValidationException ex) {
                request.setAttribute("resultados", "Validación de formulario fallida");
                Tools.anadirMensaje(request, ex.getUserMessage());
            } finally {
                request.getRequestDispatcher("/admin/administration/user_administration.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("resultados", "Formulario incorrecto");
            Tools.anadirMensaje(request, "El formulario recibido no es correcto");
            request.getRequestDispatcher("/admin/administration/user_administration.jsp").forward(request, response);
        }
    }

    protected boolean validateForm(HttpServletRequest request) {
        if ((request.getParameterMap().size() >= 3) && (request.getParameter("nombre") != null)
                && (request.getParameter("estudio") != null) && (request.getParameter("edit") != null)
                && (request.getParameter("mail") != null)) {
            return true;
        }
        return false;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendError(404);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet para la edición de usuarios";
    }

    private void actualizarEncargadoCitas(PersistenceInterface persistencia, Usuario userOld, Usuario userNew) {
    }

    private Usuario usuarioActualizar(Usuario userOld, Usuario userNew) {
        String email;
        if (userNew == null) {
            email = userOld.getMail();
        } else {
            if (userNew.getMail().equals(userOld.getMail())) {
                email = userOld.getMail();
            } else {
                email = userNew.getMail();
            }
        }
        String nombre;
        if (userNew == null) {
            nombre = userOld.getNombre();
        } else {
            if (userNew.getNombre().equals(userOld.getNombre())) {
                nombre = userOld.getNombre();
            } else {
                nombre = userNew.getNombre();
            }
        }
        int estudio;
        if (userNew == null) {
            estudio = userOld.getEstudio();
        } else {
            if (userNew.getEstudio()==userOld.getEstudio()) {
                estudio = userOld.getEstudio();
            } else {
                estudio = userNew.getEstudio();
            }
        }
        int nTelefono;
        if (userNew == null) {
            nTelefono = userOld.getnTelefono();
        } else {
            if (userNew.getnTelefono() != userOld.getnTelefono()) {
                nTelefono = userOld.getnTelefono();
            } else {
                nTelefono = userNew.getnTelefono();
            }
        }
        int idEstudio;
        if (userNew == null) {
            idEstudio = userOld.getIdEStudio();
        } else {
            if (userNew.getIdEStudio() != userOld.getIdEStudio()) {
                idEstudio = userOld.getIdEStudio();
            } else {
                idEstudio = userNew.getIdEStudio();
            }
        }
        String pass;
        if (userNew == null) {
            pass = userOld.getPass();
        } else {
            if (userNew.getPass().equals(userOld.getPass())) {
                pass = userOld.getPass();
            } else {
                pass = userNew.getPass();
            }
        }
        int viaCom;
        if (userNew == null) {
            viaCom = userOld.getViaCom();
        } else {
            if (userNew.getViaCom() == userOld.getViaCom()) {
                viaCom = userOld.getViaCom();
            } else {
                viaCom = userNew.getViaCom();
            }
        }
        return new Usuario(nombre, email, pass, nTelefono, viaCom, idEstudio);
    }
}
