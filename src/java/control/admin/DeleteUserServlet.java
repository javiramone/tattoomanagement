package control.admin;

import control.Tools;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Usuario;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import persistencia.PersistenceInterface;

public class DeleteUserServlet
        extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        block13:
        {
            if (this.validar(request)) {
                try {
                    String email = Tools.validateEmail((String) request.getParameter("user"));
                    request.setAttribute("resultados", (Object) "Resultados de la operaci\u00f3n");
                    PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
                    Usuario user = persistencia.getUser(email);
                    if (user != null) {
                        boolean ok = persistencia.delUser(email);
                        if (ok) {
                            Tools.anadirMensaje((HttpServletRequest) request, (String) "El usuario se ha borrado correctamente");
                            if (email.equals((String) request.getSession().getAttribute("usuario"))) {
                                response.sendRedirect("/logout");
                                return;
                            }
                        } else {
                            Tools.anadirMensaje((HttpServletRequest) request, (String) "Ha ocurrido un error borrando el usuario");
                        }
                        break block13;
                    }
                    Tools.anadirMensaje((HttpServletRequest) request, (String) "El usuario seleccionado no se ha encontrado, imposible borrar");
                } catch (IntrusionException ex) {
                    Tools.anadirMensaje((HttpServletRequest) request, (String) ex.getUserMessage());
                } catch (ValidationException ex) {
                    Tools.anadirMensaje((HttpServletRequest) request, (String) ex.getUserMessage());
                } finally {
                    request.getRequestDispatcher("/logout").forward((ServletRequest) request, (ServletResponse) response);
                }
            }
            response.sendError(404);
        }
    }

    protected boolean validar(HttpServletRequest request) {
        if (request.getParameterMap().size() >= 1 && request.getParameter("user") != null) {
            return true;
        }
        return false;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        this.processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendError(404);
    }

    public String getServletInfo() {
        return "Servlet para el borrado de un usuario";
    }
}
