package control.admin;

import control.Tools;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cita;
import org.owasp.esapi.errors.IntrusionException;
import persistencia.PersistenceInterface;

public class EditCitaFechaServlet
        extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendError(404);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
        try {
            String codigoCita = request.getParameter("cita");
            //Obtenemos la cita
            Cita cita = persistencia.getCita(codigoCita);

            //Buscamos huecos para una cita con la duración de la cita que queremos cambiar
            ArrayList<Cita> citasDisponibles = persistencia.getPosiblesCitas(cita.getDuracionInt(), 3, 2, cita.getIdUsuario());

        //Seleccionamos la primera cita        
            //Seleccionamos la fechahora del hueco encontrado
            String fechahora = citasDisponibles.get(0).getFechaHoraString();

            //Editamos la cita con la nueva fecha y hora
            boolean exito = persistencia.updateFechaCita(cita.getCodigo(), fechahora);

            request.setAttribute("resultados", "Resultados de la operación");
            if (exito == true) {
                Tools.anadirMensaje(request, "La cita se ha editado correctamente");
            } else {
                Tools.anadirMensaje(request, "Ha ocurrido un error al modificar la cita. Compruebe la fecha y hora");
            }
            request.getRequestDispatcher("/index.jsp").forward(request, response);
        } catch (IntrusionException ex) {
            request.setAttribute("resultados", "Intrusión detectada");
            Tools.anadirMensaje(request, ex.getUserMessage());
            request.getRequestDispatcher("/citas/citas.jsp").forward(request, response);
        }
    }

    public String getServletInfo() {
        return "Servlet encargado de la edición de citas";
    }
}
