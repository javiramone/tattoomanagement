package control.admin;

import control.Tools;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import persistencia.PersistenceInterface;

public class DeleteCitaServlet
        extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (validar(request) == true) {
            request.setAttribute("resultados", "Resultados de la operación");
            PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
            boolean ok = persistencia.delCita(request.getParameter("cita"));
            if (ok == true) {
                String path = "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/citas/"+ request.getParameter("cita") + ".jpg";
                File file = new File (path);
                file.delete();
                Tools.anadirMensaje(request, "La cita ha sido borrada correctamente");
            } else {
                Tools.anadirMensaje(request, "Ha ocurrido un error borrando la cita");
            }
            RequestDispatcher borrado = request.getRequestDispatcher("/citas/citas.jsp");
            borrado.forward(request, response);
        } else {
            response.sendError(404);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendError(404);
    }

    protected boolean validar(HttpServletRequest request) {
        if ((request.getParameterMap().size() >= 1) && (request.getParameter("cita") != null)) {
            return Tools.validateUUID(request.getParameter("cita"));
        }
        return false;
    }

    public String getServletInfo() {
        return "Servlet para borrado de una cita";
    }
}
