package control.admin;

import control.Tools;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cita;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import persistencia.PersistenceInterface;

@MultipartConfig
public class EditCitaServlet
        extends HttpServlet {

    private boolean validateForm(HttpServletRequest request)
            throws IOException, ServletException {
        if ((request.getParts().size() >= 7) && (request.getPart("codigo") != null) && (request.getPart("nombreContacto") != null)
                && (request.getPart("precio") != null) && (request.getPart("telefonoContacto") != null)
                && (request.getPart("fechaHora") != null) && (request.getPart("duracion") != null)
                && (request.getPart("fianza") != null)) {
            return Tools.validateUUID(Tools.getcontentPartText(request.getPart("codigo")));
        }
        return false;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendError(404);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
        if (validateForm(request) == true) {
            try {
                String codigoCita = Tools.getcontentPartText(request.getPart("codigo"));
                String nombreContacto = Tools.validateProdText(Tools.getcontentPartText(request.getPart("nombreContacto")), 70, "NombreContacto");
                int telefonoContacto = Tools.validateTelefono(Tools.getcontentPartText(request.getPart("telefonoContacto")), "TelefonoContacto");
                double precio = Tools.validatePrecio(Tools.getcontentPartText(request.getPart("precio")), "Precio");
                int fianza = Integer.parseInt(request.getParameter("fianza"));
                int recordatorio = Integer.parseInt(request.getParameter("recordatorio"));
                double duracion = Tools.validateDuracion(Tools.getcontentPartText(request.getPart("duracion")), "Duracion");
                double duracionAntigua = Tools.validateDuracion(Tools.getcontentPartText(request.getPart("duracionAntigua")), "DuracionAntigua");
                String detalles = Tools.getContentTextArea(request.getPart("detail"));
                String fechaHora = Tools.getcontentPartText(request.getPart("fechaHora"));
                String fechaHoraOld = Tools.getcontentPartText(request.getPart("fechaAntigua"));
                List<Date> dates = new ArrayList();
                dates = persistencia.getFechas();

                String idUsuario = Tools.getcontentPartText(request.getPart("idUsuario"));

                Tools.validateHTML(detalles);
                String rutaImagen = "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/citas/"+ codigoCita + ".jpg";
                if (Tools.existeElFichero(rutaImagen)) {
                    Tools.borrarImagenDeProdructoDelSistemaDeFicheros(rutaImagen);
                }
                if (request.getPart("foto").getSize() > 0L) {
                    if ((!request.getPart("foto").getContentType().contains("image"))
                            || (request.getPart("foto").getSize() > 8388608L)) {
                        request.setAttribute("resultados", "Archivo no válido");
                        Tools.anadirMensaje(request, "Solo se admiten archivos de tipo imagen");
                        Tools.anadirMensaje(request, "El tamaño máximo de archivo son 8 Mb");
                        request.getRequestDispatcher("/admin/administration/addcita.jsp").forward(request, response);
                        return;
                    }
                    String fileName = "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/citas/"+ codigoCita + ".jpg";

                    boolean ok = Tools.guardarImagenDeProdructoEnElSistemaDeFicheros(request.getPart("foto").getInputStream(), fileName);
                    if (!ok) {
                        request.setAttribute("resultados", "Fallo al guardar archivo");
                        Tools.anadirMensaje(request, "Ocurrio un error guardando la imagen");
                        request.getRequestDispatcher("/admin/administration/addcita.jsp").forward(request, response);
                        return;
                    }
                }
                Cita cita = new Cita(codigoCita, fechaHora, duracion, precio, telefonoContacto, nombreContacto, fianza, recordatorio, detalles, idUsuario);
                boolean ok = persistencia.updateCita(cita.getCodigo(), cita);
                request.setAttribute("resultados", "Resultados de la operación");
                if (ok == true) {
                    Tools.anadirMensaje(request, "La cita se ha editado correctamente");
                } else {
                    Tools.anadirMensaje(request, "Ha ocurrido un error al modificar la cita. Compruebe la fecha y hora");
                }
                request.getRequestDispatcher("/citas/citas.jsp").forward(request, response);
            } catch (IntrusionException ex) {
                request.setAttribute("resultados", "Intrusión detectada");
                Tools.anadirMensaje(request, ex.getUserMessage());
                request.getRequestDispatcher("/citas/citas.jsp").forward(request, response);
            } catch (ValidationException ex) {
                request.setAttribute("resultados", "Validación de formulario fallida");
                Tools.anadirMensaje(request, ex.getUserMessage());
                request.getRequestDispatcher("/citas/citas.jsp").forward(request, response);
            }
        }
    }

    public String getServletInfo() {
        return "Servlet encargado de la edición de citas";
    }
}
