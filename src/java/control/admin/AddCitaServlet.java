package control.admin;

import control.Tools;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cita;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import persistencia.PersistenceInterface;

@MultipartConfig
public class AddCitaServlet
        extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendError(404);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
        if (validateForm(request)) {
            try {
                String nombreContacto = Tools.validateProdText(Tools.getcontentPartText(request.getPart("nombreContacto")), 70, "NombreContacto");
                int telefonoContacto = Tools.validateTelefono(Tools.getcontentPartText(request.getPart("telefonoContacto")), "TelefonoContacto");
                double precio = Tools.validatePrecio(Tools.getcontentPartText(request.getPart("precio")), "Precio");
                int fianza = Integer.parseInt(request.getParameter("fianza"));
                int recordatorio = Integer.parseInt(request.getParameter("recordatorio"));
                double duracion = Tools.validateDuracion(Tools.getcontentPartText(request.getPart("duracion")), "Duracion");
                String detalles = Tools.getContentTextArea(request.getPart("detail"));

                String fechaHora = Tools.getcontentPartText(request.getPart("fechaHora"));
                String idUsuario = Tools.getcontentPartText(request.getPart("idUsuario"));
                String codigoCita = Tools.generaUUID();
                if (request.getPart("foto").getSize() > 0L) {
                    if ((!request.getPart("foto").getContentType().contains("image"))
                            || (request.getPart("foto").getSize() > 8388608L)) {
                        request.setAttribute("resultados", "Archivo no válido");
                        Tools.anadirMensaje(request, "Solo se admiten archivos de tipo imagen");
                        Tools.anadirMensaje(request, "El tamaño máximo de archivo son 8 Mb");
                        request.getRequestDispatcher("/admin/administration/addcita.jsp").forward(request, response);
                        return;
                    }
                    //String fileName = getServletContext().getRealPath("/images/citas/") + "/" + codigoCita + ".jpg";
                    String fileName = "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/citas/"+ codigoCita + ".jpg";

                    boolean ok = Tools.guardarImagenDeProdructoEnElSistemaDeFicheros(request.getPart("foto").getInputStream(), fileName);
                    if (!ok) {
                        request.setAttribute("resultados", "Fallo al guardar archivo");
                        Tools.anadirMensaje(request, "Ocurrio un error guardando la imagen");
                        request.getRequestDispatcher("/index.jsp").forward(request, response);
                        return;
                    }
                }
                Cita cita = new Cita(codigoCita, fechaHora, duracion, precio, telefonoContacto, nombreContacto, fianza, recordatorio, detalles, idUsuario);
                boolean ok = persistencia.addCita(cita, true);
                request.setAttribute("resultados", "Resultados de la operación false");
                if (ok == true) {
                    Tools.anadirMensaje(request, "La cita se ha añadido correctamente");
                } else {
                    Tools.anadirMensaje(request, "Ha ocurrido un error al añadir la cita. Compruebe la fecha y/o hora de la cita e inténtelo de nuevo");
                }
                request.getRequestDispatcher("/citas/citas.jsp").forward(request, response);
            } catch (IntrusionException ex) {
                request.setAttribute("resultados", "Intrusión detectada");
                Tools.anadirMensaje(request, ex.getUserMessage());
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            } catch (ValidationException ex) {
                request.setAttribute("resultados", "Datos de formulario no válidos");
                Tools.anadirMensaje(request, ex.getUserMessage());
                request.getRequestDispatcher("/citas/citas.jsp").forward(request, response);
            }
        } else {
            request.setAttribute("resultados", "Formulario no válido");
            Tools.anadirMensaje(request, "El formulario recibido no tiene los campos esperados");
            request.getRequestDispatcher("/citas/citas.jsp").forward(request, response);
        }
    }

    private boolean validateForm(HttpServletRequest request)
            throws IOException, ServletException {
        if ((request.getPart("nombreContacto") != null)
                && (request.getPart("precio") != null) && (request.getPart("telefonoContacto") != null)
                && (request.getPart("duracion") != null)) {
            return true;
        }
        return false;
    }

    public String getServletInfo() {
        return "Servlet para añadir citas";
    }
}
