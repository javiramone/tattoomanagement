package control.citas;

import control.SendMail;
import control.Tools;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.Timer;
import java.util.TimerTask;
import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;
import modelo.Cita;
import persistencia.PersistenceInterface;

public class RecordatorioCitas
        extends HttpServlet
        implements ServletContextListener {

    private Long getIntervalMiliSeconds(int pIntervalMinutos) {
        long IntMiliSeconds = 60000 * pIntervalMinutos;
        return Long.valueOf(IntMiliSeconds);
    }

    protected void mandarEmailRecordatorio(Date dateToday, ServletContextEvent servletContextEvent) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        PersistenceInterface persistencia = (PersistenceInterface) servletContextEvent.getServletContext().getAttribute("persistence");

        ArrayList<String> contactos = new ArrayList();
        contactos = persistencia.getUserDateToday();
        if (contactos != null) {
            StringBuilder tablaEmail = new StringBuilder();
            Iterator<String> contactosIterator = contactos.iterator();
            while (contactosIterator.hasNext()) {
                String dir = (String) contactosIterator.next();
                ArrayList<Cita> citas = new ArrayList();
                if (persistencia.getUser(dir).getViaCom() != 2) {
                    citas = persistencia.getCitasUserToday(dir);
                    Iterator<Cita> citasIterator = citas.iterator();
                    while (citasIterator.hasNext()) {
                        Cita citaTabla = (Cita) citasIterator.next();
                        tablaEmail.append(generarFilaTabla(citaTabla));
                    }
                    SendMail mailConfig = (SendMail) servletContextEvent.getServletContext().getAttribute("EmailSend");
                    Session mailSession = mailConfig.startSession((Authenticator) servletContextEvent.getServletContext().getAttribute("autorizacionMail"));
                    String contenido = Tools.leerArchivoClassPath("/plantillaRecordatorio.html");
                    contenido = contenido.replace("&DATE", dateFormat.format(dateToday));
                    contenido = contenido.replace("&LISTA", generateTableCitas(tablaEmail));
                    MimeMessage mensaje = mailConfig.newMail("Citas de hoy", dir, contenido, mailSession);
                    if (mensaje == null) {
                        System.out.println("Error enviando el mail");
                    } else {
                        boolean ok = mailConfig.sendEmail(mensaje, mailSession);
                        if (ok == true) {
                            System.out.println("EMAIL ENVIADO! :) ");
                        } else {
                            System.out.println("Error enviando el mail");
                        }
                    }
                }
            }
        } else {
            System.out.println("No hay citas para hoy");
        }
    }

    //Manda sms a los usuarios con citas en el día
    protected void mandarEmailRecordatorio2(Date dateToday, ServletContextEvent servletContextEvent) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        PersistenceInterface persistencia = (PersistenceInterface) servletContextEvent.getServletContext().getAttribute("persistence");

        ArrayList<String> contactos = new ArrayList();
        //Recoge los usuarios con citas registradas para el dia
        contactos = persistencia.getUserDateToday();
        if (contactos != null) {
            StringBuilder tablaEmail = new StringBuilder();
            Iterator<String> contactosIterator = contactos.iterator();
            while (contactosIterator.hasNext()) {
                String dir = (String) contactosIterator.next();
                ArrayList<Cita> citas = new ArrayList();
                if (persistencia.getUser(dir).getViaCom() >= 2) {
                    citas = persistencia.getCitasUserToday(dir);
                    Iterator<Cita> citasIterator = citas.iterator();
                    while (citasIterator.hasNext()) {
                        Cita citaTabla = (Cita) citasIterator.next();
                        tablaEmail.append("\nNombre: " + citaTabla.getNombreContacto() + " - Telefono: " + citaTabla.getTelefonoContacto() + " - Hora: " + citaTabla.getHoraString());
                    }
                    SendMail mailConfig = (SendMail) servletContextEvent.getServletContext().getAttribute("EmailSend");
                    Session mailSession = mailConfig.startSession((Authenticator) servletContextEvent.getServletContext().getAttribute("autorizacionMail"));
                    String contenido = dateFormat.format(dateToday) + tablaEmail;
                    if (persistencia.getTelefonoUser(dir) != 0) {
                        String mailSMS = persistencia.getTelefonoUser(dir) + "@echoemail.net";
                        MimeMessage mensaje = mailConfig.newMail("Sus citas de hoy", mailSMS, contenido, mailSession);
                        if (mensaje == null) {
                            System.out.println("Error enviando el SMS");
                        } else {
                            boolean ok = mailConfig.sendEmail(mensaje, mailSession);
                            if (ok == true) {
                                System.out.println("SMS ENVIADO! :) ");
                            } else {
                                System.out.println("Error enviando el SMS");
                            }
                        }
                    }
                }
            }
        } else {
            System.out.println("No hay citas para hoy");
        }
    }

    //Manda un SMS a los clientes con citas en el día
    protected void mandaSmsCliente(ServletContextEvent servletContextEvent) {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        PersistenceInterface persistencia = (PersistenceInterface) servletContextEvent.getServletContext().getAttribute("persistence");

        ArrayList<Integer> telefonos = new ArrayList<Integer>();
        //Recoge las citas del dia
        telefonos = persistencia.getTelefonosCitasToday();
        if (telefonos != null) {
            Iterator<Integer> telefonosIterator = telefonos.iterator();
            while (telefonosIterator.hasNext()) {
                int telefonoCliente = (Integer) telefonosIterator.next();
                String contenido = persistencia.getEstudioCitaToday(telefonoCliente);
                //if (recordatorio = true){ mandar SMS } else {no mandar}
                SendMail mailConfig = (SendMail) servletContextEvent.getServletContext().getAttribute("EmailSend");
                Session mailSession = mailConfig.startSession((Authenticator) servletContextEvent.getServletContext().getAttribute("autorizacionMail"));
                String mailSMS = telefonoCliente + "@echoemail.net";
                MimeMessage mensaje = mailConfig.newMail("Recordatorio ", mailSMS, contenido, mailSession);
                if (mensaje == null) {
                    System.out.println("Error enviando el SMS");
                } else {
                    boolean ok = mailConfig.sendEmail(mensaje, mailSession);
                    if (ok == true) {
                        System.out.println("SMS ENVIADO! :) ");
                    } else {
                        System.out.println("Error enviando el SMS");
                    }
                }
            }
        } else {
            System.out.println("No hay citas para hoy");
        }
    }

    protected StringBuilder generarFilaTabla(Cita cita) {
        StringBuilder sb = new StringBuilder();
        sb.append("<tr class=\"contentTable\">");
        sb.append("\n");

        sb.append("<td>");
        sb.append(cita.getNombreContacto());
        sb.append("</td>");

        sb.append("<td>");
        sb.append(cita.getHoraString());
        sb.append("</td>");

        sb.append("<td>");
        sb.append(Tools.roundDouble(cita.getPrecio()));
        sb.append(" &euro;</td>");

        sb.append("<td>");
        sb.append(cita.getTelefonoContacto());
        sb.append("</td>");

        sb.append("</tr>");
        sb.append("\n");

        return sb;
    }

    protected String generateTableCitas(StringBuilder filas) {
        StringBuilder sb = new StringBuilder();
        sb.append("<table border=\"0\" align=\"center\" width=\"90%\">");
        sb.append("\n");
        sb.append("<tr class=\"headerTable\" style=\"font-weight: bold;\"><td>Nombre Contacto</td><td>Hora</td><td>Precio</td><td>Teléfono Contacto</td></tr>");
        sb.append("\n");
        sb.append("<tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>");
        sb.append("\n");
        sb.append(filas);
        sb.append("\n");
        sb.append("</table>");
        return sb.toString();
    }

    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("RecordatorioCitas Listener initialized.");

        TimerTask vodTimer = new VodTimerTask(sce);

        Timer timer = new Timer();

        long InvertalMiliSeconds = getIntervalMiliSeconds(1440).longValue();

        timer.schedule(vodTimer, 5000L, InvertalMiliSeconds);
    }

    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Recordatorio Citas Listener has been shutdown");
    }

    class VodTimerTask extends TimerTask {

        private ServletContextEvent sce;

        private VodTimerTask(ServletContextEvent sce) {
            this.sce = sce;
        }

        public void run() {
            mandarEmailRecordatorio(new Date(), sce);
            mandarEmailRecordatorio2(new Date(), sce);
            mandaSmsCliente(sce);
            System.out.println("Mail enviado, espera 24 horas para mandar el siguiente ");
        }
    }
}
