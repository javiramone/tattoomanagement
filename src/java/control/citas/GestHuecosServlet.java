package control.citas;

import control.Tools;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cita;
import persistencia.PersistenceInterface;

@MultipartConfig
public class GestHuecosServlet
        extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
        if (validateForm(request)) {
            int duracion = Integer.parseInt(request.getParameter("duracionest"));
            int horario = Integer.parseInt(request.getParameter("horario"));
            int festivo = Integer.parseInt(request.getParameter("festivo"));
            String idUsuario = request.getParameter("idUsuario");
            boolean seleccionado = false;

            ArrayList<Cita> citasDisponibles = persistencia.getPosiblesCitas(duracion, horario, festivo, idUsuario);
            if ((citasDisponibles == null) || (citasDisponibles.size() <= 0)) {
                Tools.anadirMensaje(request, "No se han encontrado huecos disponibles");
            } else {
                Tools.anadirMensaje(request, "Se ha encontrado " + citasDisponibles.size() + " huecos");
            }
            request.setAttribute("resultadosBusquedaHuecos", citasDisponibles);
            request.getRequestDispatcher("/citas/huecos_administration.jsp").forward(request, response);
        }
    }

    private boolean validateForm(HttpServletRequest request) {
        if ((request.getParameter("duracionest") != null) && (request.getParameter("horario") != null)
                && (request.getParameter("festivo") != null)) {
            return true;
        }
        return false;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendError(404);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet encargado de la búsqueda de citas dentro de la lista";
    }
}
