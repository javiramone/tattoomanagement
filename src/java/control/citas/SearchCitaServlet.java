package control.citas;

import control.Tools;
import java.io.IOException;
import java.util.Map;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cita;
import persistencia.PersistenceInterface;

public class SearchCitaServlet
        extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PersistenceInterface persistence = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
        if (validateForm(request)) {
            String redirect = request.getParameter("redirect");
            if ((!redirect.equals("/citas/citas.jsp")) && (!redirect.equals("/admin/administration/citas_administration.jsp"))) {
                response.sendError(404);
                return;
            }
            String destination = devolverCampo(request.getParameter("campo"));
            String term = request.getParameter("term");
            String idUsuario = request.getParameter("idUsuario");
            Map<String, Cita> resultados = persistence.searchCita(destination, term, idUsuario);
            request.setAttribute("resultados", "Resultados de la búsqueda");
            if ((resultados == null) || (resultados.size() <= 0)) {
                Tools.anadirMensaje(request, "No se han encontrado coincidencias para la búsqueda de " + term + " en el apartado " + destination + ". Se mostrarán todas las citas");
            } else {
                Tools.anadirMensaje(request, "Se han encontrado " + resultados.size() + " coincidencias");
            }
            request.setAttribute("resultadosBusqueda", resultados);
            RequestDispatcher mostrar = request.getRequestDispatcher(redirect);
            mostrar.forward(request, response);
        }
    }

    private boolean validateForm(HttpServletRequest request) {
        if ((request.getParameterMap().size() >= 4) && (request.getParameter("term") != null)
                && (request.getParameter("campo") != null) && (request.getParameter("search") != null)
                && (request.getParameter("redirect") != null) && ((request.getParameter("campo").equals("NombreContacto")) || (request.getParameter("campo").equals("Duracion"))
                || (request.getParameter("campo").equals("Detalles")) || (request.getParameter("campo").equals("TelefonoContacto")))) {
            return true;
        }
        return false;
    }

    private String devolverCampo(String form) {
        if (form.equals("TelefonoContacto")) {
            return "TelefonoContacto";
        }
        if (form.equals("NombreContacto")) {
            return "NombreContacto";
        }
        if (form.equals("Duracion")) {
            return "Duracion";
        }
        if (form.equals("Detalles")) {
            return "Detalles";
        }
        return "NombreContacto";
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendError(404);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    public String getServletInfo() {
        return "Servlet encargado de la búsqueda de citas dentro de la lista";
    }
}
