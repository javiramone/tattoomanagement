package control;

import java.io.IOException;
import javax.mail.Authenticator;
import javax.mail.Session;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import modelo.Cita;
import modelo.Usuario;
import org.owasp.esapi.errors.IntrusionException;
import org.owasp.esapi.errors.ValidationException;
import persistencia.PersistenceInterface;

public class RegisterServlet
        extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.sendError(404);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (validateForm(request) == true) {
            try {
                String name = request.getParameter("name");

                String email = Tools.validateEmail(request.getParameter("email"));
                String pass = Tools.validatePass(request.getParameter("pass"));
                String repeatPass = Tools.validatePass(request.getParameter("repeatPass"));
                int idEstudio = Integer.parseInt(request.getParameter("estudio"));
                int nTelefono;
                if (request.getParameter("nTelefono") == null) {
                    nTelefono = 0;
                } else {
                    nTelefono = Integer.parseInt(request.getParameter("nTelefono"));
                }
                String[] viasCom = request.getParameterValues("viaCom");
                int viaCom = 0;
                if (viasCom != null) {
                    viaCom = 0;
                    for (int x = 0; x < viasCom.length; x++) {
                        viaCom += Integer.parseInt(viasCom[x]);
                    }
                } else {
                    viaCom = 0;
                }
                if (pass.equals(repeatPass)) {
                    String passMd = Tools.generateMD5Signature(pass + pass.toLowerCase());
                    Usuario user = new Usuario(name, email, passMd, nTelefono, viaCom, idEstudio);

                    PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");

                    boolean citapr = persistencia.addCita(new Cita(Tools.generaUUID(), "2001/01/01 00:00", 0.0D, 0.0D, 0, null, 0, 0, null, email), false);
                    if (citapr) {
                        boolean ok = persistencia.addUser(user);
                        if (ok) {
                            request.setAttribute("resultados", "Usuario registrado");
                            Tools.anadirMensaje(request, "Su usuario ha sido registrado correctamente");
                            Tools.anadirMensaje(request, "Ya puede realizar las compras que desee con su usuario, dispone de un formulario de login en esta misma página");
                            Tools.anadirMensaje(request, "Puede acceder a su panel de usuario desde el menú después de iniciar sesión");
                            if ((user.getnTelefono() != 0) && (user.getViaCom() == 2 || user.getViaCom()==3)) {
                                mandarEmail2(request, user, pass);
                            }
                            if ((user.getMail()!= null) && (user.getViaCom() == 1 || user.getViaCom()==3)) {
                                mandarEmail(request, user);
                            }
                        }
                    } else {
                        request.setAttribute("resultados", "Error en el registro");
                        Tools.anadirMensaje(request, "Ha habido un error con el registro");
                    }
                } else {
                    request.setAttribute("resultados", "Datos incorrectos");
                    Tools.anadirMensaje(request, "La contraseña no coincide con su repeticion");
                }
            } catch (IntrusionException ex) {
                request.setAttribute("resultados", "Detectada una intrusión");
                Tools.anadirMensaje(request, ex.getUserMessage());
            } catch (ValidationException ex) {
                request.setAttribute("resultados", "Error en el formulario");
                Tools.anadirMensaje(request, ex.getUserMessage());
            }
        } else {
            request.setAttribute("resultados", "Ocurrio un error en el registro");
            Tools.anadirMensaje(request, "El formulario enviado no es correcto");
        }
        request.getRequestDispatcher("/login.jsp").forward(request, response);
    }

    protected boolean validateForm(HttpServletRequest request) {
        if ((request.getParameterMap().size() >= 6) && (request.getParameter("name") != null) && (request.getParameter("estudio") != null)
                && (request.getParameter("email") != null) && (request.getParameter("pass") != null)
                && (request.getParameter("repeatPass") != null) && (request.getParameter("register") != null)) {
            return true;
        }
        return false;
    }

    protected boolean mandarEmail(HttpServletRequest request, Usuario user) {
        SendMail mailConfig = (SendMail) request.getServletContext().getAttribute("EmailSend");
        PersistenceInterface persistencia = (PersistenceInterface) request.getServletContext().getAttribute("persistence");
        Session mailSession = mailConfig.startSession((Authenticator) request.getServletContext().getAttribute("autorizacionMail"));
        String contenido = Tools.leerArchivoClassPath("/plantillaRegistro.html");
        contenido = contenido.replace("&NAME", user.getNombre());
        contenido = contenido.replace("&EMAIL", user.getMail());
        contenido = contenido.replace("&ESTUDIO", persistencia.getEstudio(user.getEstudio()).getNombre());
        MimeMessage mensaje = mailConfig.newMail("Registro completado", user.getMail(), contenido, mailSession);
        if (mensaje == null) {
            request.setAttribute("resultados", "Error enviando mensaje");
            Tools.anadirMensaje(request, "No se pudo enviar su email, disculpe las molestias");
            return false;
        }
        boolean ok = mailConfig.sendEmail(mensaje, mailSession);
        if (ok == true) {
            Tools.anadirMensaje(request, "Se le ha enviado un email con los datos del registro");
            return true;
        }
        Tools.anadirMensaje(request, "No se puedo enviar el email con los datos del registro");
        return false;
    }

    protected boolean mandarEmail2(HttpServletRequest request, Usuario user, String pass) {
        SendMail mailConfig = (SendMail) request.getServletContext().getAttribute("EmailSend");
        Session mailSession = mailConfig.startSession((Authenticator) request.getServletContext().getAttribute("autorizacionMail"));

        String contenido = "Gracias por registrarse en TattooManagement. Sus datos de acceso son: Usuario=" + user.getMail() + " - pass: " + pass + " .";
        String mailSMS = user.getnTelefono() + "@echoemail.net";
        MimeMessage mensaje = mailConfig.newMail("Registro completado", mailSMS, contenido, mailSession);
        if (mensaje == null) {
            request.setAttribute("resultados", "Error enviando mensaje");
            Tools.anadirMensaje(request, "No se pudo enviar su email, disculpe las molestias");
            return false;
        }
        boolean ok = mailConfig.sendEmail(mensaje, mailSession);
        if (ok == true) {
            Tools.anadirMensaje(request, "Se le ha enviado un sms con los datos del registro");
            return true;
        }
        Tools.anadirMensaje(request, "No se puedo enviar el sms con los datos del registro");
        return false;
    }

    public String getServletInfo() {
        return "Servlet para el registro de usuarios";
    }
}
