package control;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Cita;
import modelo.Usuario;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import persistencia.PersistenceInterface;

/**
 * @author Javi Ramone
 */
public class GeneradorDeEstadiaticasDeCitas {

    ArrayList<Cita> historial = new ArrayList<Cita>();

    public GeneradorDeEstadiaticasDeCitas() {
        this.historial = historial;
    }

    public boolean porcentajeCitasMes(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloMes(porcentajeCitasMes(persistencia, usuario)), ruta,
                "Citas por mes", "Mes", "N_citas");
    }
    
    public boolean porcentajeCitasSemana(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloSem(porcentajeCitasSemana(persistencia, usuario)), ruta,
                "Citas por día de la semana", "Día", "N_citas");
    }

    public boolean porcentajeDineroMes(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloMes(porcentajeDineroMes(persistencia, usuario)), ruta,
                "Ganancias por mes", "Mes", "€");
    }
    
    public boolean porcentajeDineroSemana(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloSem(porcentajeDineroSemana(persistencia, usuario)), ruta,
                "Ganancias por día de la semana", "Día", "€");
    }
    
    public boolean porcentajeDuracionMes(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloMes(porcentajeDuracionMes(persistencia, usuario)), ruta,
                "Duración de citas por mes", "Mes", "horas");
    }
    
    public boolean porcentajeDuracionSemana(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloSem(porcentajeDuracionSemana(persistencia, usuario)), ruta,
                "Duración de citas por día de la semana", "Día", "horas");
    }

    public boolean porcentajeCitasMesEstudio(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloMes(porcentajeCitasMesEstudio(persistencia, usuario)), ruta,
                "Citas por mes", "Mes", "N_citas");
    }
    
    public boolean porcentajeCitasSemanaEstudio(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloSem(porcentajeCitasSemanaEstudio(persistencia, usuario)), ruta,
                "Citas por día de la semana", "Día", "N_citas");
    }

    public boolean porcentajeDinerosMesEstudio(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloMes(porcentajeDinerosMesEstudio(persistencia, usuario)), ruta,
                "Ganancias por mes", "Mes", "Ganancias");
    }
    
    public boolean porcentajeDinerosSemanaEstudio(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloSem(porcentajeDineroSemanaEstudio(persistencia, usuario)), ruta,
                "Ganancias por día de la semana", "Día", "Ganancias");
    }
    
    public boolean porcentajeDuracionesMesEstudio(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloMes(porcentajeDuracionesMesEstudio(persistencia, usuario)), ruta,
                "Duración de citas por mes", "Mes", "horas");
    }
    
    public boolean porcentajeDuracionesSemanaEstudio(PersistenceInterface persistencia, String ruta, String usuario) {
        return saveJPG(crearModeloSem(porcentajeDuracionSemanaEstudio(persistencia, usuario)), ruta,
                "Duración de citas por día de la semana", "Día", "horas");
    }

    private double[] porcentajeCitasMes(PersistenceInterface persistencia, String usuario) {
        int[] citasEnMes = new int[12];
        double[] resultados = new double[12];
        int citasMes = 0;
        for (int i = 0; i < 11; i++) {
            //                                  mes
            citasMes = persistencia.getNCitas((i + 1), usuario);
            citasEnMes[i] = citasMes;
        }
        for (int i = 0; i <= 11; i++) {
            resultados[i] = citasEnMes[i];
        }

        return resultados;
    }
    
    private int[] porcentajeCitasSemana(PersistenceInterface persistencia, String usuario) {
        int[] citasEnSemana = new int[7];
        double[] resultados = new double[7];
        int citasSem = 0;
        GregorianCalendar cal = new GregorianCalendar();	
        ArrayList<Date> fechas = persistencia.getCitasSemana(usuario);
        // Recorrer el arrayList
        for(int x=0;x<fechas.size();x++) {
            cal.setTime(fechas.get(x));
            // Comprobar el día de la semana a la que pertenece cada fecha
            // 1 = dominigo; 2 = lunes; 3 = martes; 4 = miercoles...
            int pos = cal.get(Calendar.DAY_OF_WEEK);
            // Sumar +1 en la posición que corresponda
            citasEnSemana[pos-1]++;
        }
        
        return citasEnSemana;
    }

    private double[] porcentajeDineroMes(PersistenceInterface persistencia, String usuario) {
        double[] dineros = new double[12];
        double[] resultados = new double[12];
        for (int i = 0; i < 11; i++) {
            //                                               mes
            dineros[i] += persistencia.getCitasMes(usuario, (i + 1));
        }
        for (int i = 0; i <= 11; i++) {
            resultados[i] = dineros[i];
        }
        return resultados;
    }
    
    private int[] porcentajeDineroSemana(PersistenceInterface persistencia, String usuario) {
        int[] dinerosEnSemana = new int[7];
        double[] resultados = new double[7];
        int dinerosSem = 0;
        GregorianCalendar cal = new GregorianCalendar();	
        ArrayList<Cita> citas = persistencia.getCitasDeSemana(usuario);
        // Recorrer el arrayList
        for(int x=0;x<citas.size();x++) {
            cal.setTime(citas.get(x).getFechaDate());
            // Comprobar el día de la semana a la que pertenece cada fecha
            // 1 = dominigo; 2 = lunes; 3 = martes; 4 = miercoles...
            int pos = cal.get(Calendar.DAY_OF_WEEK);
            // Sumar +1 en la posición que corresponda
            dinerosEnSemana[pos-1] = dinerosEnSemana[pos-1] + citas.get(x).getPrecioInt();
        }
        
        return dinerosEnSemana;
    }
    
    private double[] porcentajeDuracionMes(PersistenceInterface persistencia, String usuario) {
        double[] duraciones = new double[12];
        double[] resultados = new double[12];
        for (int i = 0; i < 11; i++) {
            //                                                        mes
            duraciones[i] += persistencia.getDuracionesMes(usuario, (i + 1));
        }
        for (int i = 0; i <= 11; i++) {
            resultados[i] = duraciones[i];
        }
        return resultados;
    }
    
    private int[] porcentajeDuracionSemana(PersistenceInterface persistencia, String usuario) {
        int[] duracionEnSemana = new int[7];
        GregorianCalendar cal = new GregorianCalendar();	
        ArrayList<Cita> citas = persistencia.getCitasDeSemana(usuario);
        // Recorrer el arrayList
        for(int x=0;x<citas.size();x++) {
            cal.setTime(citas.get(x).getFechaDate());
            // Comprobar el día de la semana a la que pertenece cada fecha
            // 1 = dominigo; 2 = lunes; 3 = martes; 4 = miercoles...
            int pos = cal.get(Calendar.DAY_OF_WEEK);
            // Sumar +1 en la posición que corresponda
            duracionEnSemana[pos-1] = duracionEnSemana[pos-1] + (int)citas.get(x).getDuracion();
        }
        
        return duracionEnSemana;
    }

    private double[] porcentajeCitasMesEstudio(PersistenceInterface persistencia, String usuario) {
        int[] citasEnMes = new int[12];
        double[] resultados = new double[12];
        int citasMes = 0;
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = persistencia.getEstudio(user.getIdEStudio()).getID();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el n_citas de cada mes
        for (int x = 0; x < 11; x++) {
            for (int y = 0; y < usuarios.size(); y++) {
                //                                  mes
                //citasMes = persistencia.getNCitas((i+1), usuario);
                citasEnMes[x] = +persistencia.getNCitas(x, usuarios.get(y));
            }
        }
        for (int i = 0; i <= 11; i++) {
            resultados[i] = citasEnMes[i];
        }

        return resultados;
    }
    
    private int[] porcentajeCitasSemanaEstudio(PersistenceInterface persistencia, String usuario) {
        int[] citasEnSemana = new int[7];
        int[] resultados = new int[7];
        GregorianCalendar cal = new GregorianCalendar();
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = persistencia.getEstudio(user.getIdEStudio()).getID();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el n_citas de la semana
        for (int x = 0; x < usuarios.size(); x++) {
            ArrayList<Date> fechas = persistencia.getCitasSemana(usuarios.get(x));
            for(int y=0;y<fechas.size();y++) {
                cal.setTime(fechas.get(y));
                // Comprobar el día de la semana a la que pertenece cada fecha
                // 1 = dominigo; 2 = lunes; 3 = martes; 4 = miercoles...
                int pos = cal.get(Calendar.DAY_OF_WEEK);
                // Sumar +1 en la posición que corresponda
                citasEnSemana[pos-1]++;
            }
        }
        return citasEnSemana;
    }

    private double[] porcentajeDinerosMesEstudio(PersistenceInterface persistencia, String usuario) {
        double[] dineroEnMes = new double[12];
        double[] resultados = new double[12];
        int dinerosMes = 0;
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = persistencia.getEstudio(user.getIdEStudio()).getID();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el total de ganancias de cada mes
        for (int x = 0; x < 11; x++) {
            for (int y = 0; y < usuarios.size(); y++) {
                //                                                     mes
                dineroEnMes[x] = +persistencia.getCitasMes(usuarios.get(y), x);
            }
        }
        for (int i = 0; i <= 11; i++) {
            resultados[i] = dineroEnMes[i];
        }

        return resultados;
    }
    
    private int[] porcentajeDineroSemanaEstudio(PersistenceInterface persistencia, String usuario) {
        int[] dinerosEnSemana = new int[7];
        int[] resultados = new int[7];
        GregorianCalendar cal = new GregorianCalendar();
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = persistencia.getEstudio(user.getIdEStudio()).getID();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el n_citas de la semana
        for (int x = 0; x < usuarios.size(); x++) {
            ArrayList<Cita> citas = persistencia.getCitasDeSemana(usuario);
            for(int y=0;y<citas.size();y++) {
                cal.setTime(citas.get(y).getFechaDate());
                // Comprobar el día de la semana a la que pertenece cada fecha
                // 1 = dominigo; 2 = lunes; 3 = martes; 4 = miercoles...
                int pos = cal.get(Calendar.DAY_OF_WEEK);
                // Sumar +1 en la posición que corresponda
                dinerosEnSemana[pos-1] = dinerosEnSemana[pos-1] + citas.get(y).getPrecioInt();
            }
        }
        return dinerosEnSemana;
    }
    
    private double[] porcentajeDuracionesMesEstudio(PersistenceInterface persistencia, String usuario) {
        double[] duracionEnMes = new double[12];
        double[] resultados = new double[12];
        int duracionesMes = 0;
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = persistencia.getEstudio(user.getIdEStudio()).getID();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el total de ganancias de cada mes
        for (int x = 0; x < 11; x++) {
            for (int y = 0; y < usuarios.size(); y++) {
                //                                                                mes
                duracionEnMes[x] = +persistencia.getDuracionesMes(usuarios.get(y), x);
            }
        }
        for (int i = 0; i <= 11; i++) {
            resultados[i] = duracionEnMes[i];
        }

        return resultados;
    }
    
    private int[] porcentajeDuracionSemanaEstudio(PersistenceInterface persistencia, String usuario) {
        int[] duracionEnSemana = new int[7];
        int[] resultados = new int[7];
        GregorianCalendar cal = new GregorianCalendar();
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = persistencia.getEstudio(user.getIdEStudio()).getID();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el n_citas de la semana
        for (int x = 0; x < usuarios.size(); x++) {
            ArrayList<Cita> citas = persistencia.getCitasDeSemana(usuario);
            for(int y=0;y<citas.size();y++) {
                cal.setTime(citas.get(y).getFechaDate());
                // Comprobar el día de la semana a la que pertenece cada fecha
                // 1 = dominigo; 2 = lunes; 3 = martes; 4 = miercoles...
                int pos = cal.get(Calendar.DAY_OF_WEEK);
                // Sumar +1 en la posición que corresponda
                duracionEnSemana[pos-1] = duracionEnSemana[pos-1] + (int)citas.get(y).getDuracion();
            }
        }
        return duracionEnSemana;
    }

    public int totalCitasMesEstudio(PersistenceInterface persistencia, String usuario) {
        int citasTotales = 0;
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = user.getIdEStudio();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = new ArrayList();
        usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el n_citas totales
        for (int y = 0; y < usuarios.size(); y++) {
            //System.out.println(usuarios.get(y));
            citasTotales = +persistencia.getNCitasTotales(usuarios.get(y));
        }

        return citasTotales;
    }

    public int totalDinerosMesEstudio(PersistenceInterface persistencia, String usuario) {
        int dinerosTotales = 0;
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = user.getIdEStudio();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = new ArrayList();
        usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el n_citas totales
        for (int y = 0; y < usuarios.size(); y++) {
            //System.out.println(usuarios.get(y));
            dinerosTotales = +persistencia.getNDinerosTotales(usuarios.get(y));
        }

        return dinerosTotales;
    }
    
    public int totalDuracionesMesEstudio(PersistenceInterface persistencia, String usuario) {
        int duracionesTotales = 0;
        //Recoger id del estudio del usuario
        Usuario user = persistencia.getUser(usuario);
        int idEstudio = user.getIdEStudio();
        //Recoger todos los usuarios de ese estudio
        ArrayList<String> usuarios = new ArrayList();
        usuarios = persistencia.getUsersFromEstudio(idEstudio);
        //Por cada usuario, recoger el n_citas totales
        for (int y = 0; y < usuarios.size(); y++) {
            //System.out.println(usuarios.get(y));
            duracionesTotales = +persistencia.getDuracionesTotales(usuarios.get(y));
        }

        return duracionesTotales;
    }

    private DefaultCategoryDataset crearModeloMes(double[] datos) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.setValue(datos[0], "Enero", "Ene");
        dataset.setValue(datos[1], "Febrero", "Feb");
        dataset.setValue(datos[2], "Marzo", "Mar");
        dataset.setValue(datos[3], "Abril", "Abr");
        dataset.setValue(datos[4], "Mayo", "May");
        dataset.setValue(datos[5], "Junio", "Jun");
        dataset.setValue(datos[6], "Julio", "Jul");
        dataset.setValue(datos[7], "Agosto", "Ago");
        dataset.setValue(datos[8], "Septiembre", "Sept");
        dataset.setValue(datos[9], "Octubre", "Oct");
        dataset.setValue(datos[10], "Noviembre", "Nov");
        dataset.setValue(datos[11], "Diciembre", "Dic");

        return dataset;
    }

    private DefaultCategoryDataset crearModeloSem(int[] datos) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.setValue(datos[1], "Lunes", "L");
        dataset.setValue(datos[2], "Martes", "M");
        dataset.setValue(datos[3], "Miércoles", "X");
        dataset.setValue(datos[4], "Jueves", "J");
        dataset.setValue(datos[5], "Viernes", "V");
        dataset.setValue(datos[6], "Sábado", "S");
        dataset.setValue(datos[0], "Domingo", "D");
        return dataset;
    }


    private boolean saveJPG(DefaultCategoryDataset dataset, String ruta, String titulo,
            String ejeX, String ejeY) {
        try {
            JFreeChart chart = ChartFactory.createBarChart3D(titulo, ejeX, ejeY, dataset,
                    PlotOrientation.VERTICAL, true, true, false);
            ChartUtilities.saveChartAsJPEG(new File(ruta), chart, 500, 300);
            return true;
        } catch (IOException ex) {
            Logger.getLogger(GeneradorDeEstadiaticasDeCitas.class.getName()).log(Level.SEVERE, ex.getMessage());
            return false;
        }
    }
}
