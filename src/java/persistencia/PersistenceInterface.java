package persistencia;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import modelo.Cita;
import modelo.Estudio;
import modelo.Usuario;

public interface PersistenceInterface {

    public boolean init(String var1, String var2, String var3, String var4);
    
    public Connection createConnection() throws IOException, ClassNotFoundException, SQLException;

    public boolean exit();

    public boolean addUser(Usuario var1);

    public boolean isCitaOk(Cita var1, boolean var2, boolean var3);

    public boolean addCita(Cita var1, boolean var2);

    public boolean delUser(String var1);

    public boolean delCita(String var1);

    public Usuario getUser(String var1);

    public Cita getCita(String var1);

    public Estudio getEstudio(int id);

    public ArrayList<Estudio> getEstudios();

    public boolean updateUser(String var1, Usuario var2);

    public boolean updateCita(String var1, Cita var2);

    public boolean updateFechaCita(String var1, String var2);

    public boolean updateResponsableCita(String var1, String var2);

    public boolean checkMail(String var1);

    public Map<String, Cita> getCitasAsociadas(String var1);

    public double getCitasMes(String codigoUsuario, int n_mes);

    public Map<String, Cita> getAllCitasAsociadas(String var1);

    public Map<String, Cita> getOldCitasAsociadas(String var1);

    public Map<String, Usuario> getUsers();

    public ArrayList<String> getUserDateToday();

    public ArrayList<Cita> getCitasUserToday(String var1);

    public ArrayList<String> getUsersFromEstudio(int idEstudio);

    public int getTelefonoUser(String var1);

    public ArrayList<Integer> getTelefonosCitasToday();

    public String getEstudioCitaToday(int nTelefono);

    public ArrayList<Cita> getPosiblesCitas(int var1, int var2, int var3, String var4);

    public List<Date> getFechas();

    public List<Date> getFechasAsociadas(String var1);

    public Date getFecha(String var1);

    public Map<String, Cita> searchCita(String var1, String var2, String var3);

    public Map<String, Cita> searchAllCitas(String var1, String var2, String var3);

    public Map<String, Cita> searchOldCitas(String var1, String var2, String var3);

    public boolean saveRequest(String var1, String var2, String var3, String var4, String var5, String var6, String var7);

    public ArrayList<Cita> requestSalesRecord(String campo, String term, String idUsuario);

    public int getNCitas(int mes, String user);

    public int getNCitasTotales(String user);

    public int getNDinerosTotales(String usuario);
    
    public double getDuracionesMes(String codigoUsuario, int n_mes);
    
    public int getDuracionesTotales(String usuario);
    
    public ArrayList<Date> getCitasSemana(String user);
    
    public ArrayList<Cita> getCitasDeSemana(String user);
}
