package persistencia;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Cita;
import modelo.Estudio;
import modelo.Fecha;
import modelo.Usuario;

public class PersistenceFile
        implements PersistenceInterface {

    private static final PersistenceFile persistence = new PersistenceFile();
    private static final Logger logger = Logger.getLogger(PersistenceFile.class.getName());
    private String file;
    private String historiales;
    private String fileLog;
    private String recoverFile;
    private Map<String, Cita> citas = new HashMap<String, Cita>();
    private Map<String, Usuario> usuarios = new HashMap<String, Usuario>();
    private Map<String, Fecha> fechas = new HashMap<String, Fecha>();
    List<Date> fechasDate = new ArrayList<Date>();
    private final Object lockCitas = new Object();
    private final Object lockUsuarios = new Object();
    private final Object lockFechas = new Object();
    private final Object lockDates = new Object();
    private final Object lockFechasString = new Object();
    private final Object lockLog = new Object();
    private final Date fecha = new Date();

    private PersistenceFile() {
    }

    public static PersistenceFile getInstance() {
        return persistence;
    }

    public boolean init(String datos, String historiales, String log, String recover) {
        this.file = datos;
        this.historiales = historiales;
        this.fileLog = log;
        this.recoverFile = recover;
        try {
            File archivoDatos = new File(this.file);
            if (archivoDatos.exists()) {
                FileInputStream inputStream = new FileInputStream(archivoDatos);
                ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
                this.citas = (Map) objectInputStream.readObject();
                this.usuarios = (Map) objectInputStream.readObject();
                this.fechas = (Map) objectInputStream.readObject();
            }
            return true;
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Error entrada Salida", ex);
            return false;
        } catch (ClassNotFoundException ex) {
            logger.log(Level.SEVERE, "Clase no encontrada", ex);
            return false;
        }
    }

    public boolean exit() {
        try {
            FileOutputStream outputStream = new FileOutputStream(new File(this.file));
            ObjectOutputStream objectOutput = new ObjectOutputStream(outputStream);
            objectOutput.writeObject(this.citas);
            objectOutput.writeObject(this.usuarios);
            objectOutput.writeObject(this.fechas);
            objectOutput.flush();
            objectOutput.close();
            return true;
        } catch (FileNotFoundException ex) {
            logger.log(Level.SEVERE, "Fichero no enontrado", ex);
            return false;
        } catch (IOException ex) {
            logger.log(Level.SEVERE, "Error de entrada / salida", ex);
            return false;
        }
    }

    public boolean addUser(Usuario user) {
        Object object = this.lockUsuarios;
        synchronized (object) {
            if (this.usuarios.containsKey(user.getMail())) {
                return false;
            }
            this.usuarios.put(user.getMail(), user);
            return true;
        }
    }

    public boolean addCita(Cita cita, boolean check) {
        Object object = this.lockCitas;
        synchronized (object) {
            if (this.citas.containsKey(cita.getCodigo())) {
                return false;
            }
            this.citas.put(cita.getCodigo(), cita);
            return true;
        }
    }

    public boolean delUser(String mail) {
        Object object = this.lockUsuarios;
        synchronized (object) {
            if (!this.usuarios.containsKey(mail)) {
                return false;
            }
            this.usuarios.remove(mail);
            return true;
        }
    }

    public boolean delCita(String codigo) {
        Object object = this.lockCitas;
        synchronized (object) {
            if (!this.citas.containsKey(codigo)) {
                return false;
            }
            this.citas.remove(codigo);
        }
        return true;
    }

    public Usuario getUser(String mail) {
        Object object = this.lockUsuarios;
        synchronized (object) {
            return this.usuarios.get(mail);
        }
    }

    public Cita getCita(String codigo) {
        Object object = this.lockCitas;
        synchronized (object) {
            return this.citas.get(codigo);
        }
    }

    public boolean updateUser(String mail, Usuario user) {
        Object object = this.lockUsuarios;
        synchronized (object) {
            if (!this.usuarios.containsKey(mail)) {
                return false;
            }
            this.usuarios.put(mail, user);
            return true;
        }
    }

    public boolean updateCita(String codigo, Cita cita) {
        Object object = this.lockCitas;
        synchronized (object) {
            if (!this.citas.containsKey(codigo)) {
                return false;
            }
            this.citas.put(codigo, cita);
            return true;
        }
    }

    public Map<String, Cita> getCitasAsociadas(String codigoUsuario) {
        Object object = this.lockCitas;
        synchronized (object) {
            if (this.citas.size() > 0) {
                return this.citas;
            }
            return null;
        }
    }

    public Map<String, Cita> getAllCitasAsociadas(String codigoUsuario) {
        Object object = this.lockCitas;
        synchronized (object) {
            if (this.citas.size() > 0) {
                return this.citas;
            }
            return null;
        }
    }

    public Map<String, Cita> getOldCitasAsociadas(String codigoUsuario) {
        Object object = this.lockCitas;
        synchronized (object) {
            if (this.citas.size() > 0) {
                return this.citas;
            }
            return null;
        }
    }

    public Map<String, Usuario> getUsers() {
        Object object = this.lockUsuarios;
        synchronized (object) {
            if (this.usuarios.size() > 0) {
                return this.usuarios;
            }
            return null;
        }
    }

    public ArrayList<Integer> getTelefonosCitasToday() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public String getEstudioCitaToday(int nTelefono) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Date getFecha(String mail) {
        Object object = this.lockDates;
        synchronized (object) {
            return this.fecha;
        }
    }

    public List<Date> getFechas() {
        Object object = this.lockFechasString;
        synchronized (object) {
            if (this.fechasDate.size() > 0) {
                return this.fechasDate;
            }
            return null;
        }
    }

    public Map<String, Cita> searchCita(String campo, String term, String idUsuario) {
        HashMap<String, Cita> resultados = new HashMap<String, Cita>();
        if (this.citas.isEmpty()) {
            return null;
        }
        for (Cita cita : this.citas.values()) {
            if (campo.equals("TelefonoContacto")) {
                if (!Integer.toString(cita.getTelefonoContacto()).contains(term)) {
                    continue;
                }
                resultados.put(cita.getCodigo(), cita);
                continue;
            }
            if (!campo.equals("NombreContacto") || !cita.getNombreContacto().contains(term)) {
                continue;
            }
            resultados.put(cita.getCodigo(), cita);
        }
        return resultados;
    }

    public Map<String, Cita> searchAllCitas(String campo, String term, String idUsuario) {
        HashMap<String, Cita> resultados = new HashMap<String, Cita>();
        if (this.citas.isEmpty()) {
            return null;
        }
        for (Cita cita : this.citas.values()) {
            if (campo.equals("TelefonoContacto")) {
                if (!Integer.toString(cita.getTelefonoContacto()).contains(term)) {
                    continue;
                }
                resultados.put(cita.getCodigo(), cita);
                continue;
            }
            if (!campo.equals("NombreContacto") || !cita.getNombreContacto().contains(term)) {
                continue;
            }
            resultados.put(cita.getCodigo(), cita);
        }
        return resultados;
    }

    public Map<String, Cita> searchOldCitas(String campo, String term, String idUsuario) {
        HashMap<String, Cita> resultados = new HashMap<String, Cita>();
        if (this.citas.isEmpty()) {
            return null;
        }
        for (Cita cita : this.citas.values()) {
            if (campo.equals("TelefonoContacto")) {
                if (!Integer.toString(cita.getTelefonoContacto()).contains(term)) {
                    continue;
                }
                resultados.put(cita.getCodigo(), cita);
                continue;
            }
            if (!campo.equals("NombreContacto") || !cita.getNombreContacto().contains(term)) {
                continue;
            }
            resultados.put(cita.getCodigo(), cita);
        }
        return resultados;
    }

    public boolean saveRequest(String fechaHora, String requestedURL, String remoteAddr, String remoteHost, String method, String param, String userAgent) {
        PrintWriter escritorLog = null;
        String add = fechaHora + "#" + requestedURL + "#" + remoteAddr + "#" + remoteHost + "#" + method + "#" + param + "#" + userAgent;
        if (param == null) {
            param = "";
        }
        Object object = this.lockLog;
        synchronized (object) {
            try {
                escritorLog = new PrintWriter(new FileWriter(this.fileLog, true), true);
                escritorLog.println(add);
                escritorLog.close();
                return true;
            } catch (IOException ex) {
                logger.log(Level.SEVERE, ex.getMessage());
                escritorLog.close();
                return false;
            }
        }
    }

    public boolean isCitaOk(Cita cita, boolean restar, boolean actualizar) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ArrayList<Cita> getPosiblesCitas(int duracion, int horario, int festivo, String ID) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ArrayList<String> getUserDateToday() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ArrayList<Cita> getCitasUserToday(String idUsuario) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public int getTelefonoUser(String idUsuario) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean updateResponsableCita(String mailOld, String mailNew) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public boolean checkMail(String mail) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public List<Date> getFechasAsociadas(String idUsuario) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public Estudio getEstudio(int idEstudio) {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    public ArrayList<Estudio> getEstudios() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean updateFechaCita(String var1, String var2) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Cita> requestSalesRecord(String campo, String term, String idUsuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNCitas(int mes, String user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNCitasTotales(String user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<String> getUsersFromEstudio(int idEstudio) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getCitasMes(String codigoUsuario, int n_mes) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getNDinerosTotales(String usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Connection createConnection() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public double getDuracionesMes(String codigoUsuario, int n_mes) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getDuracionesTotales(String usuario) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Date> getCitasSemana(String user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ArrayList<Cita> getCitasDeSemana(String user) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
