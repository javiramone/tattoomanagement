package persistencia;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import modelo.Cita;
import modelo.Estudio;
import modelo.Usuario;

public class PersistenceBD
        implements PersistenceInterface {

    private static final PersistenceBD instance = new PersistenceBD();
    private DataSource pool;
    private String nameBD = "tattoobd";
    private static final Logger logger = Logger.getLogger(PersistenceBD.class.getName());

    private PersistenceBD() {
    }

    public static PersistenceBD getInstance() {
        return instance;
    }

    

    public Connection createConnection() throws IOException, ClassNotFoundException, SQLException {
        Class.forName("com.mysql.jdbc.Driver");
        Connection connection;
        connection = DriverManager.getConnection("jdbc:mysql://mysql9161-env-8936334.jelastic.cloudhosted.es/tattoobd", "root", "HVHgmo88547");
        System.out.println("CONNECTION: " + connection);
 
        return connection;
    }

    public boolean checkMail(String mail) {
        Connection conexion = null;
        boolean exito = false;
        PreparedStatement select = null;
        ResultSet rs = null;
        String correo = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT Email FROM " + this.nameBD + ".Usuarios  WHERE Email=?");
            select.setString(1, mail);
            rs = select.executeQuery();
            while (rs.next()) {
                correo = rs.getString("Email");
            }
            exito = correo == null;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error seleccionando usuario", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return exito;
    }

    public boolean addUser(Usuario user) {
        PreparedStatement insert;
        boolean exito;
        Connection conexion = null;
        exito = false;
        insert = null;
        Object insert2 = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            insert = conexion.prepareStatement("INSERT INTO " + this.nameBD + ".Usuarios VALUES (?,?,?,?,?,?)");
            insert.setString(1, user.getMail());
            insert.setString(2, user.getNombre());
            insert.setString(3, user.getPass());
            insert.setInt(4, user.getnTelefono());
            insert.setInt(5, user.getViaCom());
            insert.setInt(6, user.getEstudio());
            int filasAfectadas = insert.executeUpdate();
            if (filasAfectadas == 1) {
                exito = true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error insertando usuario", ex);
        } finally {
            cerrarConexionYStatement(conexion, insert);
        }
        return exito;
    }

    public boolean isCitaOk(Cita cita, boolean restar, boolean actualizar) {
        boolean citaOk = false;
        PreparedStatement insert;
        Connection conexion = null;
        boolean cont = false;
        insert = null;
        PreparedStatement select = null;
        ResultSet rs_1 = null;
        ResultSet rs_2 = null;
        ArrayList<Cita> citasRegistradas = new ArrayList<Cita>();
        boolean fechaValida = false;
        boolean horaValida = false;
        boolean manana = false;
        boolean tarde = false;
        boolean x = false;
        boolean y = false;
        boolean z = false;
        int horasTotales = 0;
        SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
        String fechaCita = formatter1.format(cita.getFecha()).replace("/", "-");
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT COALESCE(SUM(duracion), 0) as horasTotal FROM " + this.nameBD + ".Cita WHERE " + "DATE_FORMAT(FechaHora,'%Y-%m-%d')= ? AND IdentificadorUsuario = ?");
            select.setString(1, fechaCita);
            select.setString(2, cita.getIdUsuario());
            rs_1 = select.executeQuery();
            while (rs_1.next()) {
                horasTotales = rs_1.getInt("horasTotal");
            }
            if (actualizar) {
                fechaValida = ((8 - horasTotales) >= ((int) cita.getDuracion() + 1));
            } else {
                fechaValida = ((8 - horasTotales) >= ((int) cita.getDuracion() + 1));
            }
            if (fechaValida) {
                //Selecciona las citas con la misma fecha y del mismo usuario
                select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE FechaHora LIKE ? AND IdentificadorUsuario = ? ORDER BY FechaHora");
                select.setString(1, fechaCita + "%");
                select.setString(2, cita.getIdUsuario());
                rs_2 = select.executeQuery();
                while (rs_2.next()) {
                    Cita citaRegistrada = new Cita(rs_2.getString("CodigoCita"), rs_2.getString("FechaHora"), (double) rs_2.getInt("Duracion"), rs_2.getDouble("Precio"), rs_2.getInt("TelefonoContacto"), rs_2.getString("NombreContacto"), rs_2.getInt("Fianza"), rs_2.getInt("recordatorio"), rs_2.getString("Detalles"), rs_2.getString("IdentificadorUsuario"));
                    citasRegistradas.add(citaRegistrada);
                }
                if (citasRegistradas.size() <= 0) {
                    horaValida = true;
                } else {
                    int i;
                    for (i = 0; i < citasRegistradas.size(); ++i) {
                        if (Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString()) <= 14) {
                            manana = true;
                        }
                        if (Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString()) > 15) {
                            tarde = true;
                        }
                    }
                    if (!manana && Integer.parseInt(cita.getOnlyHoraString()) < 14 && Integer.parseInt(cita.getOnlyHoraString()) >= 9) {
                        horaValida = true;
                    } else if (!tarde && Integer.parseInt(cita.getOnlyHoraString()) >= 17 && Integer.parseInt(cita.getOnlyHoraString()) < 20) {
                        horaValida = true;
                    } else if (manana && Integer.parseInt(cita.getOnlyHoraString()) < 14 && Integer.parseInt(cita.getOnlyHoraString()) >= 9) {
                        for (i = 0; i < citasRegistradas.size(); i++) {
                            if (Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString()) < 17) {
                                if (Integer.parseInt(cita.getOnlyHoraString()) + ((int) cita.getDuracion() + 1) <= Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString())
                                        || Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString()) + ((int) ((Cita) citasRegistradas.get(i)).getDuracion() + 1) <= Integer.parseInt(cita.getOnlyHoraString())) {
                                    horaValida = true;
                                } else if (Integer.parseInt(cita.getOnlyHoraString()) == Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString())) {
                                    horaValida = false;
                                } else {
                                    horaValida = false;
                                }
                            }
                        }
                    } else if (tarde && Integer.parseInt(cita.getOnlyHoraString()) >= 17 && Integer.parseInt(cita.getOnlyHoraString()) <= 20) {
                        for (i = 0; i < citasRegistradas.size(); i++) {
                            if (Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString()) < 17) {
                                if (Integer.parseInt(cita.getOnlyHoraString()) + ((int) cita.getDuracion() + 1) <= Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString())
                                        || Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString()) + ((int) ((Cita) citasRegistradas.get(i)).getDuracion() + 1) <= Integer.parseInt(cita.getOnlyHoraString())) {
                                    horaValida = true;
                                } else if (Integer.parseInt(cita.getOnlyHoraString()) == Integer.parseInt(((Cita) citasRegistradas.get(i)).getOnlyHoraString())) {
                                    horaValida = false;
                                } else {
                                    horaValida = false;
                                }
                            }
                        }
                    }
                }
                if (horaValida == true && fechaValida == true) {
                    citaOk = true;
                } else {
                    citaOk = false;
                }
            } else {
                citaOk = false;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error extrayendo fechas", ex);
        } finally {
            cerrarConexionYStatement(conexion, insert);
            cerrarResultSet(rs_1);
            cerrarResultSet(rs_2);
        }
        return citaOk;
    }

    @Override
    public ArrayList<Cita> getPosiblesCitas(int duracion, int horario, int festivo, String ID) {
        Connection conexion = null;
        int[] horasM = new int[]{9, 10, 11, 12, 13};
        int[] horasT = new int[]{17, 18, 19, 20};
        int[] horasMT = new int[]{9, 10, 11, 12, 13, 17, 18, 19, 20};
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
        String[] fechas = new String[42];
        PreparedStatement select = null;
        ResultSet rs = null;
        String posibleFecha = null;
        Cita posibleCita;
        ArrayList<Cita> posiblesCitasValidas = new ArrayList<Cita>();
        ArrayList<Cita> posiblesCitas = new ArrayList<Cita>();
        ArrayList<Cita> posiblesCitasNoValidas = new ArrayList<Cita>();
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            if (festivo == 2) {
                select = conexion.prepareStatement("SELECT Fecha FROM " + this.nameBD + ".fecha WHERE (Fecha>=curdate() AND festivo=0) ORDER BY Fecha");
            } else {
                select = conexion.prepareStatement("SELECT Fecha FROM " + this.nameBD + ".fecha WHERE (Fecha>=curdate()) ORDER BY Fecha");
            }
            rs = select.executeQuery();
            int cont = 0;
            while (rs.next() && cont < 14) {
                fechas[cont] = rs.getString("Fecha");
                cont++;
            }
            //for (int cont = 0; cont < 14; ++cont) {
            //    fechas[cont] = rs.getString("Fecha");
            //}

            if (fechas[0] != null) {
                //int j;
                String fechaHora;
                if (horario == 1) {
                    for (int i = 0; i < 14; ++i) {
                        for (int j = 0; j < 5; ++j) {
                            if (horasM[j] == 9) {
                                fechaHora = fechas[i] + " 0" + horasM[j] + ":00";
                                fechaHora = fechaHora.replace("-", "/");
                                posibleCita = new Cita(null, fechaHora, (double) duracion, 0.0, 0, null, 0, 0, null, ID);
                                boolean isCitaOk = isCitaOk(posibleCita, true, true);
                                if (!isCitaOk) {
                                    posiblesCitasNoValidas.add(posibleCita);
                                } else {
                                    posiblesCitas.add(posibleCita);
                                }
                            } else {
                                fechaHora = fechas[i] + " " + horasM[j] + ":00";
                                fechaHora = fechaHora.replace("-", "/");
                                posibleCita = new Cita(null, fechaHora, (double) duracion, 0.0, 0, null, 0, 0, null, ID);
                                boolean isCitaOk = isCitaOk(posibleCita, true, true);
                                if (!isCitaOk) {
                                    posiblesCitasNoValidas.add(posibleCita);
                                } else {
                                    posiblesCitas.add(posibleCita);
                                }
                            }
                        }
                    }
                } else if (horario == 2) {
                    for (int i = 0; i < 14; ++i) {
                        for (int j = 0; j < 4; ++j) {
                            fechaHora = fechas[i] + " " + horasT[j] + ":00";
                            fechaHora = fechaHora.replace("-", "/");
                            posibleCita = new Cita(null, fechaHora, (double) duracion, 0.0, 0, null, 0, 0, null, ID);
                            boolean isCitaOk = isCitaOk(posibleCita, true, true);
                            if (!isCitaOk) {
                                posiblesCitasNoValidas.add(posibleCita);
                            } else {
                                posiblesCitas.add(posibleCita);
                            }
                        }
                    }
                } else if (horario == 3) {
                    for (int i = 0; i < 14; ++i) {
                        for (int j = 0; j < 9; ++j) {
                            if (horasMT[j] == 9) {
                                fechaHora = fechas[i] + " 0" + horasMT[j] + ":00";
                                fechaHora = fechaHora.replace("-", "/");
                                posibleCita = new Cita(null, fechaHora, (double) duracion, 0.0, 0, null, 0, 0, null, ID);
                                boolean isCitaOk = isCitaOk(posibleCita, true, true);
                                if (!isCitaOk) {
                                    posiblesCitasNoValidas.add(posibleCita);
                                } else {
                                    posiblesCitas.add(posibleCita);
                                }
                            } else {
                                fechaHora = fechas[i] + " " + horasMT[j] + ":00";
                                fechaHora = fechaHora.replace("-", "/");
                                posibleCita = new Cita(null, fechaHora, (double) duracion, 0.0, 0, null, 0, 0, null, ID);
                                boolean isCitaOk = isCitaOk(posibleCita, true, true);
                                if (!isCitaOk) {
                                    posiblesCitasNoValidas.add(posibleCita);
                                } else {
                                    posiblesCitas.add(posibleCita);
                                }
                            }
                        }
                    }
                }
            }
            for (int x = 0; x < posiblesCitas.size(); x++) {
                if (isCitaOk((Cita) posiblesCitas.get(x), true, true)) {
                    posiblesCitasValidas.add(posiblesCitas.get(x));
                }
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error extrayendo fechas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        //else {
        //    logger.log(Level.SEVERE, "Error extrayendo fechas, ARRAY VACIO");
        //}
        return posiblesCitasValidas;
    }

    public boolean addCita(Cita cita, boolean check) {
        PreparedStatement insert;
        boolean exito;
        Connection conexion = null;
        insert = null;
        exito = false;
        boolean isCitaOk = isCitaOk(cita, true, true);
        if (!isCitaOk) {
            exito = false;
        } else {
            try {
                try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
                } catch (IOException ex) {
                    Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
                } catch (ClassNotFoundException ex) {
                    Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
                }
                insert = conexion.prepareStatement("INSERT INTO " + this.nameBD + ".Cita VALUES (?,?,?,?,?,?,?,?,?,?)");
                insert.setString(1, cita.getCodigo());
                insert.setTimestamp(2, cita.getFecha());
                insert.setDouble(3, cita.getDuracion());
                insert.setDouble(4, cita.getPrecio());
                insert.setInt(5, cita.getTelefonoContacto());
                insert.setString(6, cita.getNombreContacto());
                insert.setInt(7, cita.getFianza());
                insert.setString(8, cita.getDetalles());
                insert.setString(9, cita.getIdUsuario());
                insert.setInt(10, cita.getRecordatorio());
                int filasAfectadas = insert.executeUpdate();
                if (filasAfectadas == 1) {
                    exito = true;
                }
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "Error insertando cita", ex);
            } finally {
                cerrarConexionYStatement(conexion, insert);
            }
        }
        return exito;
    }

    public boolean delUser(String mail) {
        PreparedStatement delete = null;
        Connection conexion = null;
        boolean exito = false;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            delete = conexion.prepareStatement("DELETE FROM " + this.nameBD + ".Usuarios WHERE Email=?");
            delete.setString(1, mail);
            int filasAfectadas = delete.executeUpdate();
            if (filasAfectadas != 1) {
                exito = true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error borrando usuario", ex.getMessage());
        } finally {
            cerrarConexionYStatement(conexion, delete);
        }
        return exito;
    }

    public boolean delCita(String codigo) {
        PreparedStatement delete = null;
        Connection conexion = null;
        boolean exito = false;
        PreparedStatement select = null;
        PreparedStatement fechaBorrar = null;
        Cita citaBorrar = this.getCita(codigo);
        String fechaCita = citaBorrar.getFechaString();
        Object rs = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            delete = conexion.prepareStatement("DELETE FROM " + this.nameBD + ".Cita WHERE CodigoCita=?");
            delete.setString(1, codigo);
            int filasAfectadas = delete.executeUpdate();
            if (filasAfectadas == 1) {
                exito = true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error borrando producto o borrando sus comentarios asignados", ex);
        } finally {
            cerrarConexionYStatement(conexion, delete);
        }
        return exito;
    }

    public Usuario getUser(String mail) {
        Connection conexion = null;
        PreparedStatement select = null;
        ResultSet rs = null;
        Usuario user = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT* FROM " + this.nameBD + ".Usuarios WHERE Email=?");
            select.setString(1, mail);
            rs = select.executeQuery();
            while (rs.next()) {
                user = new Usuario(rs.getString("Nombre"), rs.getString("Email"), rs.getString("Pass"), rs.getInt("nTelefono"), rs.getInt("viaCom"), rs.getInt("idEstudio"));
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo usuario", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return user;
    }

    public ArrayList<String> getUserDateToday() {
        Connection conexion = null;
        ArrayList<String> contactos;
        ResultSet rs = null;
        PreparedStatement select = null;
        contactos = new ArrayList<String>();
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT distinct IdentificadorUsuario FROM " + this.nameBD + ".Cita WHERE " + "DATE_FORMAT(FechaHora,'%Y-%m-%d')= curdate()");
            rs = select.executeQuery();
            while (rs.next()) {
                contactos.add(rs.getString("IdentificadorUsuario"));
            }
            if (contactos.size() <= 0) {
                contactos = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo usuarios", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return contactos;
    }

    public ArrayList<String> getUsersFromEstudio(int idEstudio) {
        Connection conexion = null;
        ArrayList<String> usuarios;
        ResultSet rs = null;
        PreparedStatement select = null;
        usuarios = new ArrayList<String>();
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT distinct Email FROM " + this.nameBD + ".Usuarios"
                    + " WHERE idEstudio= ?");
            select.setInt(1, idEstudio);
            rs = select.executeQuery();
            while (rs.next()) {
                usuarios.add(rs.getString("Email"));
            }
            if (usuarios.size() < 0) {
                usuarios = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo usuarios", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return usuarios;
    }

    public int getTelefonoUser(String idUsuario) {
        int telefono = 0;
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT nTelefono FROM " + this.nameBD + ".Usuarios WHERE " + "Email=?");
            select.setString(1, idUsuario);
            rs = select.executeQuery();
            while (rs.next()) {
                telefono = rs.getInt("nTelefono");
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo usuarios", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return telefono;
    }

    public ArrayList<Cita> getCitasUserToday(String idUser) {
        ArrayList<Cita> citas = new ArrayList<Cita>();
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        Cita cita = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE DATE_FORMAT(FechaHora,'%Y-%m-%d')= curdate() AND IdentificadorUsuario=?");
            select.setString(1, idUser);
            rs = select.executeQuery();
            while (rs.next()) {
                cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                citas.add(cita);
            }
            if (citas.size() <= 0) {
                citas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo citas", ex);
        } finally {
            this.cerrarConexionYStatement(conexion, select);
            this.cerrarResultSet(rs);
        }
        return citas;
    }

    public Cita getCita(String codigo) {
        Connection conexion = null;
        PreparedStatement select = null;
        ResultSet rs = null;
        Cita cita = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE CodigoCita=?");
            select.setString(1, codigo);
            rs = select.executeQuery();
            while (rs.next()) {
                cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo producto", ex);
            cita = null;
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return cita;
    }

    public Estudio getEstudio(int id) {
        Connection conexion = null;
        PreparedStatement select = null;
        ResultSet rs = null;
        Estudio estudio = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".estudio WHERE idEstudio=?");
            select.setInt(1, id);
            rs = select.executeQuery();
            while (rs.next()) {
                estudio = new Estudio(rs.getInt("idEstudio"), rs.getString("nombreEstudio"), rs.getString("direccion"), rs.getString("email"), rs.getInt("nTelefono"));
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo estudio", ex);
            estudio = null;
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return estudio;
    }

    public boolean updateUser(String mail, Usuario user) {
        PreparedStatement update = null;
        boolean exito = false;
        Connection conexion = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            update = conexion.prepareStatement("UPDATE " + this.nameBD + ".Usuarios SET Email=?, Nombre=?, Pass=?, nTelefono=?, viaCom=?, Estudio=? WHERE Email=?");
            update.setString(1, user.getMail());
            update.setString(2, user.getNombre());
            update.setString(3, user.getPass());
            update.setInt(4, user.getnTelefono());
            update.setInt(5, user.getViaCom());
            update.setInt(6, user.getEstudio());
            update.setString(7, mail);
            int filasAfectadas = update.executeUpdate();
            if (filasAfectadas == 1) {
                exito = true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error editando usuario", ex.getMessage());
        } finally {
            cerrarConexionYStatement(conexion, update);
        }
        return exito;
    }

    public boolean updateResponsableCita(String mailOld, String mailNew) {
        Connection conexion = null;
        PreparedStatement update = null;
        boolean exito = false;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            update = conexion.prepareStatement("UPDATE " + this.nameBD + ".Cita SET IdentificadorUsuario=? WHERE IdentificadorUsuario=?");
            update.setString(1, mailNew);
            update.setString(2, mailOld);
            int filasAfectadas = update.executeUpdate();
            exito = true;
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error editando responsable de cita", ex.getMessage());
        } finally {
            cerrarConexionYStatement(conexion, update);
        }
        return exito;
    }

    public boolean updateCita(String codigo, Cita cita) {
        Connection conexion = null;
        PreparedStatement update = null;
        boolean exito = false;
        boolean exitoBorrado = false;
        PreparedStatement select = null;
        PreparedStatement delete = null;
        PreparedStatement insert = null;
        Object fechaModificar_1 = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            insert = conexion.prepareStatement("INSERT INTO " + this.nameBD + ".CitaBridge VALUES (?,?,?,?,?,?,?,?,?)");
            insert.setString(1, cita.getCodigo());
            insert.setTimestamp(2, cita.getFecha());
            insert.setDouble(3, cita.getDuracion());
            insert.setDouble(4, cita.getPrecio());
            insert.setInt(5, cita.getTelefonoContacto());
            insert.setString(6, cita.getNombreContacto());
            insert.setInt(7, cita.getFianza());
            insert.setString(8, cita.getDetalles());
            insert.setString(9, cita.getIdUsuario());
            int filasAfectadas = insert.executeUpdate();
            if (filasAfectadas == 1) {
                exito = true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error insertando cita en CitaBridge", ex);
        } finally {
            cerrarConexionYStatement(conexion, insert);
        }

        this.delCita(cita.getCodigo());
        if (!this.isCitaOk(cita, true, true)) {
            return false;
        }
        try {
            this.addCita(cita, true);
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            delete = conexion.prepareStatement("DELETE FROM " + this.nameBD + ".CitaBridge");
            int filasAfectadasBorradoPasarela = delete.executeUpdate();
            exitoBorrado = filasAfectadasBorradoPasarela == 1;
            if (exitoBorrado) {
                update = conexion.prepareStatement("UPDATE " + this.nameBD + ".Cita SET FechaHora=?, Duracion=?, Precio=?, TelefonoContacto=?, NombreContacto=?, Fianza=?," + "Detalles=?, IdentificadorUsuario=? WHERE CodigoCita=?");
                update.setTimestamp(1, cita.getFecha());
                update.setDouble(2, cita.getDuracion());
                update.setDouble(3, cita.getPrecio());
                update.setInt(4, cita.getTelefonoContacto());
                update.setString(5, cita.getNombreContacto());
                update.setInt(6, cita.getFianza());
                update.setString(7, cita.getDetalles());
                update.setString(8, cita.getIdUsuario());
                update.setString(9, codigo);
                int filasAfectadas = update.executeUpdate();
                if (filasAfectadas == 1) {
                    exito = true;
                }
            }
            logger.log(Level.SEVERE, "Error borrando cita de pasarela");
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error actualizando cita", ex);
        } finally {
            cerrarConexionYStatement(conexion, update);
        }
        return exito;
    }

    public boolean updateFechaCita(String codigo, String fechaHoraNew) {
        PreparedStatement update = null;
        boolean exito = false;
        Connection conexion = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            update = conexion.prepareStatement("UPDATE " + this.nameBD + ".Cita SET FechaHora=? WHERE CodigoCita=?");
            update.setString(1, fechaHoraNew);
            update.setString(2, codigo);
            int filasAfectadas = update.executeUpdate();
            if (filasAfectadas == 1) {
                exito = true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error editando usuario", ex.getMessage());
        } finally {
            cerrarConexionYStatement(conexion, update);
        }
        return exito;
    }

    public Map<String, Cita> getCitasAsociadas(String codigoUsuario) {
        ResultSet rs = null;
        PreparedStatement select = null;
        HashMap<String, Cita> citasAsociadas = new HashMap<String, Cita>();
        Connection conexion = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=? AND" + " DATEDIFF(NOW(), FechaHora)>=-9  AND DATEDIFF(NOW(), FechaHora)<=0");
            select.setString(1, codigoUsuario);
            rs = select.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                citasAsociadas.put(cita.getCodigo(), cita);
            }
            if (citasAsociadas.size() <= 0) {
                citasAsociadas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas asociadas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return citasAsociadas;
    }

    public Map<String, Cita> getAllCitasAsociadas(String codigoUsuario) {
        ResultSet rs = null;
        PreparedStatement select = null;
        HashMap<String, Cita> citasAsociadas = new HashMap<String, Cita>();
        Connection conexion = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=? AND Duracion>0");
            select.setString(1, codigoUsuario);
            rs = select.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                citasAsociadas.put(cita.getCodigo(), cita);
            }
            if (citasAsociadas.size() <= 0) {
                citasAsociadas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas asociadas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return citasAsociadas;
    }

    public double getCitasMes(String codigoUsuario, int n_mes) {
        ResultSet rs = null;
        PreparedStatement select = null;
        double cont = 0;
        Connection conexion = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT sum(Precio) FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=?"
                    + " AND Duracion>0"
                    + " AND DATE_FORMAT(FechaHora,'%m')=?");
            select.setString(1, codigoUsuario);
            select.setInt(2, n_mes);
            rs = select.executeQuery();
            rs.next();
            cont = rs.getDouble(1);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas asociadas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return cont;
    }
    
    public double getDuracionesMes(String codigoUsuario, int n_mes) {
        ResultSet rs = null;
        PreparedStatement select = null;
        double cont = 0;
        Connection conexion = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT sum(Duracion) FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=?"
                    + " AND Duracion>0"
                    + " AND DATE_FORMAT(FechaHora,'%m')=?");
            select.setString(1, codigoUsuario);
            select.setInt(2, n_mes);
            rs = select.executeQuery();
            rs.next();
            cont = rs.getDouble(1);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas asociadas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return cont;
    }

    public Map<String, Cita> getOldCitasAsociadas(String codigoUsuario) {
        ResultSet rs = null;
        PreparedStatement select = null;
        HashMap<String, Cita> citasAsociadas = new HashMap<String, Cita>();
        Connection conexion = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=?" + "AND DATEDIFF(NOW(), FechaHora)>1");
            select.setString(1, codigoUsuario);
            rs = select.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                citasAsociadas.put(cita.getCodigo(), cita);
            }
            if (citasAsociadas.size() <= 0) {
                citasAsociadas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas asociadas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return citasAsociadas;
    }

    public Map<String, Usuario> getUsers() {
        HashMap<String, Usuario> usuarios = new HashMap<String, Usuario>();
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT* FROM " + this.nameBD + ".Usuarios");
            rs = select.executeQuery();
            while (rs.next()) {
                Usuario user = new Usuario(rs.getString("Nombre"), rs.getString("Email"), rs.getString("Pass"), rs.getInt("nTelefono"), rs.getInt("viaCom"), rs.getInt("idEstudio"));
                usuarios.put(user.getMail(), user);
            }
            if (usuarios.size() <= 0) {
                usuarios = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo los usuarios", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return usuarios;
    }

    public ArrayList<Integer> getTelefonosCitasToday() {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        ArrayList<Integer> telefonosClientes = new ArrayList<Integer>();
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT TelefonoContacto FROM " + this.nameBD + ".Cita WHERE DATE_FORMAT(FechaHora,'%Y-%m-%d')= curdate() AND recordatorio = 1");
            rs = select.executeQuery();
            while (rs.next()) {
                int nTelefono = rs.getInt("TelefonoContacto");
                telefonosClientes.add(nTelefono);
            }
            if (telefonosClientes.size() <= 0) {
                telefonosClientes = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return telefonosClientes;
    }

    public ArrayList<Estudio> getEstudios() {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        ArrayList<Estudio> estudios = new ArrayList<Estudio>();
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".estudio");
            rs = select.executeQuery();
            while (rs.next()) {
                Estudio estudio = new Estudio(rs.getInt("idEstudio"), rs.getString("nombreEstudio"), rs.getString("direccion"), rs.getString("email"), rs.getInt("nTelefono"));
                estudios.add(estudio);
            }
            if (estudios.size() <= 0) {
                estudios = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo estudios", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return estudios;
    }

    //Devuelve el texto en String del mensaje recordatorio
    public String getEstudioCitaToday(int nTelefono) {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        PreparedStatement select_2 = null;
        ResultSet rs_2 = null;
        String Estudio = null;
        String fechaHora = null;
        StringBuilder result = new StringBuilder();
        result.append("Tattoo Management le recuerda que tiene cita hoy en el estudio ");
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT distinct nombreEstudio FROM " + this.nameBD + ".estudio, " + this.nameBD + ".Usuarios, " + this.nameBD + ".Cita"
                    + " WHERE " + this.nameBD + ".estudio.idEstudio = " + this.nameBD + ".Usuarios.idEstudio"
                    + " AND " + this.nameBD + ".Usuarios.Email = " + this.nameBD + ".Cita.IdentificadorUsuario"
                    + " AND DATE_FORMAT(" + this.nameBD + ".Cita.FechaHora,'%Y-%m-%d')= curdate()"
                    + " AND " + this.nameBD + ".Cita.TelefonoContacto = ?");
            select.setInt(1, nTelefono);
            rs = select.executeQuery();
            rs.next();
            Estudio = rs.getString("nombreEstudio");
            result.append(Estudio);
            result.append(" a la hora: ");
            select_2 = conexion.prepareStatement("SELECT FechaHora FROM " + this.nameBD + ".Cita"
                    + " WHERE DATE_FORMAT(" + this.nameBD + ".Cita.FechaHora,'%Y-%m-%d')= curdate()");
            rs_2 = select_2.executeQuery();
            rs_2.next();
            fechaHora = rs_2.getString("FechaHora");
            result.append(fechaHora);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return result.toString();
    }

    public Date getFecha(String fechaStr) {
        Date fecha = new Date();
        Connection conexion = null;
        PreparedStatement select = null;
        ResultSet rs = null;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd");
        SimpleDateFormat formatter3 = new SimpleDateFormat("yyyy-MM-dd");
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT fechaHora FROM " + this.nameBD + ".Cita  WHERE fechaHora LIKE ?");
            select.setString(1, "%" + fechaStr + "%");
            rs = select.executeQuery();
            while (rs.next()) {
                try {
                    Date date_1;
                    String fecha_string = formatter.format(rs.getTimestamp("FechaHora"));
                    fecha = date_1 = formatter.parse(fecha_string);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return fecha;
    }

    public List<Date> getFechas() {
        ArrayList<Date> fechas = new ArrayList<Date>();
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT fechaHora FROM " + this.nameBD + ".Cita");
            rs = select.executeQuery();
            while (rs.next()) {
                try {
                    String fecha_string = formatter.format(rs.getTimestamp("FechaHora"));
                    Date date_1 = formatter.parse(fecha_string);
                    fechas.add(date_1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (fechas.size() <= 0) {
                fechas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
            fechas = null;
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return fechas;
    }

    public List<Date> getFechasAsociadas(String idUsuario) {
        ResultSet rs = null;
        ArrayList<Date> fechas = new ArrayList<Date>();
        PreparedStatement select = null;
        Connection conexion = null;
        SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT fechaHora FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=?");
            select.setString(1, idUsuario);
            rs = select.executeQuery();
            while (rs.next()) {
                try {
                    String fecha_string = formatter.format(rs.getTimestamp("FechaHora"));
                    Date date_1 = formatter.parse(fecha_string);
                    fechas.add(date_1);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            if (fechas.size() <= 0) {
                fechas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
            fechas = null;
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }

        return fechas;
    }

    public Map<String, Cita> searchCita(String campo, String term, String idUsuario) {
        Connection conexion = null;
        ResultSet rs = null;
        HashMap<String, Cita> citas = new HashMap<String, Cita>();
        PreparedStatement select = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=? " + "AND DATEDIFF(NOW(), FechaHora)>=-9  AND DATEDIFF(NOW(), FechaHora)<=0 AND " + campo + " LIKE ?");
            select.setString(1, idUsuario);
            select.setString(2, "%" + term + "%");
            rs = select.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                citas.put(cita.getCodigo(), cita);
            }
            if (citas.size() <= 0) {
                citas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error buscando cita", ex);
            citas = null;
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return citas;
    }

    public Map<String, Cita> searchAllCitas(String campo, String term, String idUsuario) {
        Connection conexion = null;
        ResultSet rs = null;
        HashMap<String, Cita> citas = new HashMap<String, Cita>();
        PreparedStatement select = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=? " + "AND " + campo + " LIKE ?");
            select.setString(1, idUsuario);
            select.setString(2, "%" + term + "%");
            rs = select.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                citas.put(cita.getCodigo(), cita);
            }
            if (citas.size() <= 0) {
                citas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error buscando cita", ex);
            citas = null;
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return citas;
    }

    public Map<String, Cita> searchOldCitas(String campo, String term, String idUsuario) {
        Connection conexion = null;
        ResultSet rs = null;
        HashMap<String, Cita> citas = new HashMap<String, Cita>();
        PreparedStatement select = null;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT * FROM " + this.nameBD + ".Cita WHERE IdentificadorUsuario=? " + "AND DATEDIFF(NOW(), FechaHora)>1 AND " + campo + " LIKE ?");
            select.setString(1, idUsuario);
            select.setString(2, "%" + term + "%");
            rs = select.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                citas.put(cita.getCodigo(), cita);
            }
            if (citas.size() <= 0) {
                citas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error buscando cita", ex);
            citas = null;
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return citas;
    }

    public ArrayList<Cita> requestSalesRecord(String campo, String term, String idUsuario) {
        if (campo.equals("1") == true) {
            campo = "'1'";
        }
        Connection conexion = null;
        PreparedStatement select = null;
        ResultSet rs = null;
        ArrayList<Cita> historial = new ArrayList<Cita>();
        try {
            Calendar cal = Calendar.getInstance(new Locale("es", "ES"));
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT* FROM " + this.nameBD
                    + ".Cita WHERE DATE_FORMAT(FechaHora,'%Y-%m-%d')>= curdate()"
                    + " AND IdentificadorUsuario=? AND " + campo + "=?");
            select.setString(1, idUsuario);
            select.setString(2, term);
            rs = select.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"),
                        rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"),
                        rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"),
                        rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                historial.add(cita);
            }

            if (historial.size() <= 0) {
                historial = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo historial de citas", ex);
            historial = null;
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return historial;
    }

    public boolean saveRequest(String fechaHora, String requestedURL, String remoteAddr,
            String remoteHost, String method, String param, String userAgent) {
        Connection conexion = null;
        PreparedStatement insert = null;
        boolean exito = false;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            insert = conexion.prepareStatement("INSERT INTO " + this.nameBD + ".log VALUES (?,?,?,?,?,?,?)");
            insert.setString(1, fechaHora);
            insert.setString(2, requestedURL);
            insert.setString(3, remoteAddr);
            insert.setString(4, remoteHost);
            insert.setString(5, method);
            insert.setString(6, param);
            insert.setString(7, userAgent);

            int filasAfectadas = insert.executeUpdate();
            if (filasAfectadas == 1) {
                exito = true;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo historial de citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, insert);
        }
        return exito;
    }

    private void cerrarConexionYStatement(Connection conexion, Statement... statements) {
        try {
            conexion.close();
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error al cerrar una conexión a la base de datos", ex);
        } finally {
            for (Statement statement : statements) {
                if (statement != null) {
                    try {
                        statement.close();
                    } catch (SQLException ex) {
                        logger.log(Level.SEVERE, "Error al cerrar un statement", ex);
                    }
                }
            }
        }
    }

    private void cerrarResultSet(ResultSet... results) {
        for (ResultSet rs : results) {
            if (rs == null) {
                continue;
            }
            try {
                rs.close();
                continue;
            } catch (SQLException ex) {
                logger.log(Level.SEVERE, "Error al cerrar un resultset", ex);
            }
        }
    }

    //ESTADISTICAS
    public int getNCitasTotales(String usuario) {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        int cont = 0;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT count(*) FROM " + this.nameBD + ".Cita WHERE DATE_FORMAT(FechaHora,'%Y')<= curdate() AND IdentificadorUsuario =?");
            select.setString(1, usuario);
            rs = select.executeQuery();
            rs.next();
            cont = rs.getInt(1);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return cont;
    }

    public int getNDinerosTotales(String usuario) {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        int cont = 0;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT sum(Precio) FROM " + this.nameBD + ".Cita WHERE DATE_FORMAT(FechaHora,'%Y')<= curdate() AND IdentificadorUsuario =?");
            select.setString(1, usuario);
            rs = select.executeQuery();
            rs.next();
            cont = rs.getInt(1);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return cont;
    }
    
    public int getDuracionesTotales(String usuario) {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        int cont = 0;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT sum(Duracion) FROM " + this.nameBD + ".Cita WHERE DATE_FORMAT(FechaHora,'%Y')<= curdate() AND IdentificadorUsuario =?");
            select.setString(1, usuario);
            rs = select.executeQuery();
            rs.next();
            cont = rs.getInt(1);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return cont;
    }

    public int getNCitas(int mes, String user) {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
        int cont = 0;
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("SELECT count(*) FROM " + this.nameBD + ".Cita "
                    + "WHERE DATE_FORMAT(FechaHora,'%Y')=year(curdate()) AND "
                    + "DATE_FORMAT(FechaHora,'%m')= ? AND "
                    + "IdentificadorUsuario = ?");
            select.setInt(1, mes);
            select.setString(2, user);
            rs = select.executeQuery();
            rs.next();
            cont = rs.getInt(1);
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return cont;
    }
    
    public ArrayList<Date> getCitasSemana(String user) {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
	ArrayList<Date> fechas = new ArrayList<Date>();
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("Select Date_Format(FechaHora ,'%Y-%m-%d') from " + this.nameBD + ".Cita "
                    + "WHERE IdentificadorUsuario = ?"
                    + "AND FechaHora >= curdate() "
                    + "AND FechaHora <= (curdate() + interval 7 day);");
            select.setString(1, user);
            rs = select.executeQuery();
            while (rs.next()) {
                Date fecha = rs.getDate("Date_Format(FechaHora ,'%Y-%m-%d')");
                fechas.add(fecha);
            }
            if (fechas.size() <= 0) {
                fechas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return fechas;
    }

    public ArrayList<Cita> getCitasDeSemana(String user) {
        PreparedStatement select = null;
        Connection conexion = null;
        ResultSet rs = null;
	ArrayList<Cita> citas = new ArrayList<Cita>();
        try {
            try {
                //conexion = this.pool.getConnection();
                conexion = this.createConnection();
            } catch (IOException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(PersistenceBD.class.getName()).log(Level.SEVERE, null, ex);
            }
            select = conexion.prepareStatement("Select * from " + this.nameBD + ".Cita "
                    + "WHERE IdentificadorUsuario = ?"
                    + "AND FechaHora >= curdate() "
                    + "AND FechaHora <= (curdate() + interval 7 day);");
            select.setString(1, user);
            rs = select.executeQuery();
            while (rs.next()) {
                Cita cita = new Cita(rs.getString("CodigoCita"), rs.getString("FechaHora"), rs.getDouble("Duracion"), rs.getDouble("Precio"), rs.getInt("TelefonoContacto"), rs.getString("NombreContacto"), rs.getInt("Fianza"), rs.getInt("recordatorio"), rs.getString("Detalles"), rs.getString("IdentificadorUsuario"));
                citas.add(cita);
            }
            if (citas.size() <= 0) {
                citas = null;
            }
        } catch (SQLException ex) {
            logger.log(Level.SEVERE, "Error obteniendo las citas", ex);
        } finally {
            cerrarConexionYStatement(conexion, select);
            cerrarResultSet(rs);
        }
        return citas;
    }
    
    @Override
    public boolean exit() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean init(String var1, String var2, String var3, String var4) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
