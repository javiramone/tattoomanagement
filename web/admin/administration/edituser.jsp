<%@page import="modelo.Estudio"%>
<%@page import="org.owasp.esapi.errors.ValidationException"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="control.Tools"%>
<%@page import="modelo.Usuario"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%if (validar (request) == false){
    response.sendError(404);
    return;
} %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon" href="images/icons/icon.png"> </link>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Modificar Usuario</title>

<script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
<script type="text/javascript" src="/scripts/vanadium.js"></script>
<link rel="stylesheet" type="text/css" href="/css/validacion.css" media="screen, tv, projection" />
<link rel="icon" href="images/icons/icon.png"> </link>
<link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
<script language="JavaScript">
    function habilita(){
        if( $('#bysms').prop('checked') ) {
            $("#nTelefono").removeAttr("disabled");
        } else {
            $("#nTelefono").prop("disabled", true);
        }
     
    }
</script>
</head>

<body>
<!-- Contenedor principal-->
<div id="siteBox">
    
    <!--Cabecera-->
    <%@include file="/WEB-INF/include/header.jsp" %>
    
    <!-- Contenido de la pagina -->
    <div id="content">

    <!-- Menu Izquiero : side bar links/news/search/etc. -->
    <%@include file="/WEB-INF/include/menuAdministracion.jsp" %>

    <!-- Contenido de la columna derecha -->
    <%@include file="/WEB-INF/include/resultados.jsp" %>
    
    <div id="contentRight">
        <% PersistenceInterface persistencia = (PersistenceInterface)application.getAttribute("persistence");
        Usuario user = persistencia.getUser(request.getParameter("user"));
        ArrayList <String> opciones = new ArrayList<String>(); 
        ArrayList <Estudio> estudios = persistencia.getEstudios();
        if (estudios!=null){
            ArrayList <Integer> valuesEstudio = new ArrayList<Integer>();
            ArrayList <String> nombresEstudio = new ArrayList<String>(); 
            for (int i=0; i<estudios.size(); i++){
                valuesEstudio.add(estudios.get(i).getID());
                nombresEstudio.add(estudios.get(i).getNombre());
            }
            for (int i=0;i<valuesEstudio.size();i++)
            {
                if (valuesEstudio.get(i)==user.getIdEStudio()){
                    opciones.add("<option selected value="+valuesEstudio.get(i)+">"+nombresEstudio.get(i)+"</option>");
                } else {
                    opciones.add("<option value="+valuesEstudio.get(i)+">"+nombresEstudio.get(i)+"</option>");
                }          
            }
        } else {
            opciones.add("<option value=-ERROR->-Error-</option>");
        }       
        if (user == null){
            request.setAttribute("resultados", "Usuario no encontrado");
            Tools.anadirMensaje(request, "El usuario que desea editar no ha sido encontrado"); %>
            <jsp:forward page="/admin/administration/user_administration.jsp" />
        <% }else{ %>
        <p>
            <span class="header">Editar Usuario</span>
            <p>
                <form action="/admin/administration/edituser" method="post" name="editUser">
                    <b>Email</b> <br />
                    <input type="text" name="mail" maxlength="70" size="50" value="<%=user.getMail()%>"><br /><br />
                    <b>Nombre</b> <br />
                    <input type="text" name="nombre" maxlength="100" size="50" class=":alpha :only_on_blur" value="<%= user.getNombre() %>" /><br /><br />
                    
                    <b>Estudio</b><br />
                    <select name="estudio" class=":required :only_on_blur" value="<%=user.getIdEStudio()%>">
                        <option> Seleccionar Estudio </option>
                        <%=opciones%>
                    </select><br /><br />
                    
                    <b>Indicar vías de comunicación</b><br />
                    <% if (user.getViaCom()==0) { %>
                        <input id="byemail" name="viaCom" type="checkbox" value="1"/> E-mail
                        <input id="bysms" name="viaCom" type="checkbox" value="2" onclick="habilita()"/> SMS
                        <br />
                    <%} else if (user.getViaCom()==1) { %>
                        <input id="byemail" name="viaCom" type="checkbox" value="1" checked/> E-mail
                        <input id="bysms" name="viaCom" type="checkbox" value="2" onclick="habilita()"/> SMS
                        <br />
                    <%} else if (user.getViaCom()==2) { %>
                        <input id="byemail" name="viaCom" type="checkbox" value="1"/> E-mail
                        <input id="bysms" name="viaCom" type="checkbox" value="2" onclick="habilita()" checked/> SMS
                        <br />
                    <%} else if (user.getViaCom()==3) { %>
                        <input id="byemail" name="viaCom" type="checkbox" value="1" checked/> E-mail
                        <input id="bysms" name="viaCom" type="checkbox" value="2" onclick="habilita()" checked/> SMS
                        <br />
                    <%} %>
                    <b>Número de Teléfono</b> <br />
                    <input id="nTelefono" name="nTelefono" type="text" size="30" maxlength="9" value="<%=user.getnTelefono()%>" disabled class=":only_on_blur"/>
                    <br /><br />
                    
                    <b> Contrase&ntilde;a</b> (Campo requerido)<br />
                    <input id="pass" name="pass" type="password" size="25" maxlength="20" class=":password :only_on_blur"/><br /><br />

                    <b>Reescriba la contrase&ntilde;a por seguridad </b>(Campo requerido)<br />
                    <input name="repeatPass" type="password" size="25" maxlength="20" class=":same_as;pass :only_on_blur"/><br /><br />
                    
                    <input type="submit" name="edit" value="Editar usuario"/>       
                </form>
            </p>
        </p>
        <% } %>
      <!-- Crea las esquinas redondeadas abajo -->
      <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>
    </div>
</div>

<!-- Pie de pagina -->
<%@include file="/WEB-INF/include/footer.jsp" %>

</div>

</body>
</html>

<%--Funcion para validar la entrada en esta jsp con los parametros necesarios--%>
<%! private boolean validar (HttpServletRequest request){
    if (request.getParameterMap().size()>=1 && request.getParameter("user")!= null ){
        try{
            Tools.validateEmail(request.getParameter("user"));
            return true;
        }catch (ValidationException ex){
            return false;
        }
    }else{
        return false;
    }
} %>

<%! String menuInicio = ""; %>
<%! String menuCitas = ""; %>
<%! String menuLogin = ""; %>
<%! String menuPreferencias = "class=\"active\""; %>
<%! String menuAbout = ""; %>