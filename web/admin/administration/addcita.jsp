<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.ArrayList"%>
<%@page import="modelo.Cita"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon" href="images/icons/icon.png"> </link>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>A&ntilde;adir cita</title>

<script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
<link rel="stylesheet" type="text/css" href="/css/validacion.css" media="screen, tv, projection" />
<script type="text/javascript" src="/scripts/jquery.js"></script>
<script type="text/javascript" src="/scripts/jquery.datetimepicker.full.min.js"></script>
<script type="text/javascript" src="/scripts/jquery.datetimepicker.js"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" media="screen, tv, projection" />

<link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
</head>

    
 
<!-- Contenedor principal-->
<div id="siteBox">

	<!--Cabecera-->
    <%@include file="/WEB-INF/include/header.jsp" %>


  <!-- Contenido de la pagina -->
  <div id="content">

  <!-- Menu Izquiero : side bar links/news/search/etc. -->
  <%@include file="/WEB-INF/include/menuAdministracion.jsp" %>

    <!-- Contenido de la columna derecha -->
    <div id="contentRight">
        <%-- <%@include file="/WEB-INF/include/resultados.jsp" %> --%>
        <p>
            <span class="header">A&ntilde;adir cita</span>
        </p>

        <form name="addcita" method="post" action="/admin/administration/addcita" enctype="multipart/form-data" >
            <b>Identificador Usuario</b> <br />
            <input type="text" name="idUsuario" maxlength="70" size="50" value="<%=session.getAttribute("MailUsuario")%>" readonly/><br /><br />
            <b>Nombre Contacto</b> <br />
            <input type="text" name="nombreContacto" maxlength="70" size="50" class=":alpha :required :only_on_blur" /><br /><br />
            <b>Precio</b> <br />
            <input type="text" name="precio" maxlength="10" size="13" class=":number :required :only_on_blur" /><br /><br />
            <b>Duración</b> <br />
            <input type="text" name="duracion" maxlength="2" size="8" class=":number :required :only_on_blur" /><br /><br />
            <b>Teléfono Contacto</b> <br />
            <input type="text" name="telefonoContacto" maxlength="9" size="9" class=":digits :required :only_on_blur" /><br /><br />
            <b>Fianza</b>&nbsp;(Opcional)<br />
            <input type="radio" name="fianza" value="1" /> SI
            <input type="radio" name="fianza" value="0" checked/> NO <br /><br />
            <b>Recordatorio a cliente</b>&nbsp;(Opcional)<br />
            <input type="radio" name="recordatorio" value="1" /> SI
            <input type="radio" name="recordatorio" value="0" checked/> NO <br /><br />
            <b>Escoja una foto</b>&nbsp;(Opcional)<br />
            <input type="file" name="foto" /><br /><br /> 
            <b>Fecha</b> <br />
            <input id="datetimepicker" name="fechaHora" type="text" /><br /><br />
            <script>
                <%  //OBTENEMOS TODAS LAS FECHAS DE LA BD 
                    List <Date> dates=new ArrayList<Date>();
                    dates = ((PersistenceInterface) application.getAttribute("persistence")).getFechasAsociadas(session.getAttribute("MailUsuario").toString());
                %>
                var tamano='<%=dates.size()%>';
                var SelectedDates=[tamano];
                <%int i;%>
                <% SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
                SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd"); 
                for(i=0; i<dates.size(); i++) {%>
                    SelectedDates['<%=i%>'] = new Date('<%=formatter2.format(dates.get(i))%>');   
                <%} %>
                            
                $("#prueba").html(SelectedDates[1].getFullYear().toString()+"/"+(SelectedDates[1].getMonth()+1).toString()+"/"+SelectedDates[1].getDate().toString());
                jQuery('#datetimepicker').datetimepicker({
                    formatDate: "Y/m/d", 
                    minDate:0,
                    onGenerate:function( ct ){
                        for (var j=0; j<SelectedDates.length; j++){
                            $('[data-year="'+SelectedDates[j].getFullYear().toString()+'"][data-month="'+SelectedDates[j].getMonth().toString()+'"][data-date="'+SelectedDates[j].getDate()+'"]').addClass('xdsoft_highlighted_default');
                        };
                        jQuery(this).find('.xdsoft_date.xdsoft_weekend').addClass('xdsoft_disabled');
                    }
                });
                $.datetimepicker.setLocale('es');
            </script>
            <b>Detalles</b> (Opcional)<br />  
            <textarea name="detail" cols="60" rows="15" class=":only_on_blur"></textarea><br /> <br />
            <input type="submit" name="sendCita" value="Enviar datos" />
            <input type="reset" name="limpiar" value="Borrar datos del formulario" />
        </form>
      <!-- Crea las esquinas redondeadas abajo -->
      <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom" />

    </div>
</div>

<!-- Pie de pagina -->
<%@include file="/WEB-INF/include/footer.jsp" %>

</div>

</body>

</html>

<%! String menuInicio = ""; %>
<%! String menuCitas = ""; %>
<%! String menuLogin = ""; %>
<%! String menuPreferencias = "class=\"active\""; %>
<%! String menuAbout = ""; %>
