<%@page import="java.lang.String"%>
<%@page import="modelo.Cita"%>
<%@page import="control.GeneradorDeEstadiaticasDeCitas"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@ page contentType="text/html; charset=UTF-8" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="icon" href="images/icons/icon.png"> </link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Estad&iacute;sticas</title>
        <script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
        <script>
            $(document).ready(function(){
                $("#studio_stats_btn").click(function(){
                    $("#stats_user").toggle("slow", function(){
                        $("#stats_estudio").toggle("slow");
                    });     
                });
                $("#user_stats_btn").click(function(){
                    $("#stats_estudio").toggle("slow", function(){
                        $("#stats_user").toggle("slow");
                    });     
                });
                $("#semana_stats_btn_1").click(function(){
                    $("#ncitas_mes").toggle("slow", function(){
                        $("#ncitas_semana").toggle("slow");
                    });     
                });
                $("#mes_stats_btn_1").click(function(){
                    $("#ncitas_semana").toggle("slow", function(){
                        $("#ncitas_mes").toggle("slow");
                    });     
                });
                $("#semana_stats_btn_2").click(function(){
                    $("#dinero_mes").toggle("slow", function(){
                        $("#dinero_semana").toggle("slow");
                    });     
                });
                $("#mes_stats_btn_2").click(function(){
                    $("#dinero_semana").toggle("slow", function(){
                        $("#dinero_mes").toggle("slow");
                    });     
                });
                $("#semana_stats_btn_3").click(function(){
                    $("#duracion_mes").toggle("slow", function(){
                        $("#duracion_semana").toggle("slow");
                    });     
                });
                $("#mes_stats_btn_3").click(function(){
                    $("#duracion_semana").toggle("slow", function(){
                        $("#duracion_mes").toggle("slow");
                    });     
                });
                $("#semana_stats_btn_4").click(function(){
                    $("#ncitas_mes_estudio").toggle("slow", function(){
                        $("#ncitas_semana_estudio").toggle("slow");
                    });     
                });
                $("#mes_stats_btn_4").click(function(){
                    $("#ncitas_semana_estudio").toggle("slow", function(){
                        $("#ncitas_mes_estudio").toggle("slow");
                    });     
                });
                $("#semana_stats_btn_5").click(function(){
                    $("#dinero_mes_estudio").toggle("slow", function(){
                        $("#dinero_semana_estudio").toggle("slow");
                    });     
                });
                $("#mes_stats_btn_5").click(function(){
                    $("#dinero_semana_estudio").toggle("slow", function(){
                        $("#dinero_mes_estudio").toggle("slow");
                    });     
                });
                $("#semana_stats_btn_6").click(function(){
                    $("#duracion_mes_estudio").toggle("slow", function(){
                        $("#duracion_semana_estudio").toggle("slow");
                    });     
                });
                $("#mes_stats_btn_6").click(function(){
                    $("#duracion_semana_estudio").toggle("slow", function(){
                        $("#duracion_mes_estudio").toggle("slow");
                    });     
                });
            });
        </script>
    </head>
    <body>
        <!-- Contenedor principal-->
        <div id="siteBox">

            <!--Cabecera-->
            <%@include file="/WEB-INF/include/header.jsp" %>

            <!-- Contenido de la pagina -->
            <div id="content">

                <!-- Menu Izquiero : side bar links/news/search/etc. -->
                <%@include file="/WEB-INF/include/menuAdministracion.jsp" %>

                <!-- Contenido de la columna derecha -->
                <div id="contentRight">
                    <%@include file="/WEB-INF/include/resultados.jsp" %>

                    <div id="stats_user"> 
                        <p>
                            <span class="header" >Estad&iacute;sticas de usuario</span>
                        </p>
                        <p><a id="studio_stats_btn">Estadísticas de estudio</a></p>
                        
                        <div id="ncitas_mes">
                            <p><a id="semana_stats_btn_1">Estadísticas de la semana</a></p>
                            <% GeneradorDeEstadiaticasDeCitas genEstad = new GeneradorDeEstadiaticasDeCitas();
                                if (genEstad.porcentajeCitasMes(((PersistenceInterface) application.getAttribute("persistence")), 
                                    "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_citas_mes.jpg",
                                    //application.getRealPath("/images/stats/") + "\\stats_citas_mes.jpg", 
                                    request.getParameter("user").toString()) == true) {%>

                                    <p><span class="headerTable" style="text-align: center" >Citas por mes</span></p>
                                    <p><img src="/TattooManagement/images/stats/stats_citas_mes.jpg" alt="porcentaje citas mes" /></p>
                                    <p>Citas registradas totales: <%= ((PersistenceInterface)application.getAttribute("persistence")).getNCitasTotales(request.getParameter("user").toString()) %></p>
                                    <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="ncitas_semana" style='display:none;'>
                            <p><a id="mes_stats_btn_1">Estadísticas del mes</a></p>
                            <% if (genEstad.porcentajeCitasSemana(((PersistenceInterface) application.getAttribute("persistence")), 
                                    "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_citas_semana.jpg",
                                    //application.getRealPath("/images/stats/") + "\\stats_citas_mes.jpg", 
                                    request.getParameter("user").toString()) == true) {%>

                                    <p><span class="headerTable" style="text-align: center" >Citas por día de la semana</span></p>
                                    <p><img src="/TattooManagement/images/stats/stats_citas_semana.jpg" alt="porcentaje citas semana" /></p>
                                    <p>Citas registradas totales: <%= ((PersistenceInterface)application.getAttribute("persistence")).getNCitasTotales(request.getParameter("user").toString()) %></p>
                                    <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="dinero_mes">
                            <p><a id="semana_stats_btn_2">Estadísticas de la semana</a></p>
                            <% if (genEstad.porcentajeDineroMes(((PersistenceInterface) application.getAttribute("persistence")), 
                                "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_dinero_mes.jpg",
                                //application.getRealPath("/images/stats/") + "\\stats_dinero_mes.jpg", 
                                request.getParameter("user").toString()) == true) {%>
                                <p><span class="headerTable" style="text-align: center" >Ganancias por mes</span></p>
                                <p><img src="/TattooManagement/images/stats/stats_dinero_mes.jpg" alt="porcentaje dinero mes" /></p>
                                <p>Ganancias totales: <%= ((PersistenceInterface)application.getAttribute("persistence")).getNDinerosTotales(request.getParameter("user").toString()) %> €</p>
                                <br />
                        <% } else {%>
                            <p>Error generando el gr&aacute;fico</p>
                        <% }%>
                        </div>
                        <div id="dinero_semana" style='display:none;'>
                            <p><a id="mes_stats_btn_2">Estadísticas del mes</a></p>
                            <% if (genEstad.porcentajeDineroSemana(((PersistenceInterface) application.getAttribute("persistence")), 
                                "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_dinero_semana.jpg",
                                //application.getRealPath("/images/stats/") + "\\stats_dinero_mes.jpg", 
                                request.getParameter("user").toString()) == true) {%>
                                <p><span class="headerTable" style="text-align: center" >Ganancias por día de la semana</span></p>
                                <p><img src="/TattooManagement/images/stats/stats_dinero_semana.jpg" alt="porcentaje dinero semana" /></p>
                                <p>Ganancias totales: <%= ((PersistenceInterface)application.getAttribute("persistence")).getNDinerosTotales(request.getParameter("user").toString()) %> €</p>
                                <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="duracion_mes">
                            <p><a id="semana_stats_btn_3">Estadísticas de la semana</a></p>
                            <% if (genEstad.porcentajeDuracionMes(((PersistenceInterface) application.getAttribute("persistence")), 
                                "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_duracion_mes.jpg",
                                //application.getRealPath("/images/stats/") + "\\stats_dinero_mes.jpg", 
                                request.getParameter("user").toString()) == true) {%>
                                <p><span class="headerTable" style="text-align: center" >Duración de citas por mes</span></p>
                                <p><img src="/TattooManagement/images/stats/stats_duracion_mes.jpg" alt="porcentaje duracion mes" /></p>
                                <p>Total horas invertidas: <%= ((PersistenceInterface)application.getAttribute("persistence")).getDuracionesTotales(request.getParameter("user").toString()) %></p>
                                <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="duracion_semana" style='display:none;'>
                            <p><a id="mes_stats_btn_3">Estadísticas del mes</a></p>
                            <% if (genEstad.porcentajeDuracionSemana(((PersistenceInterface) application.getAttribute("persistence")), 
                                "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_duracion_semana.jpg",
                                //application.getRealPath("/images/stats/") + "\\stats_dinero_mes.jpg", 
                                request.getParameter("user").toString()) == true) {%>
                                <p><span class="headerTable" style="text-align: center" >Duración de citas por día de la semana</span></p>
                                <p><img src="/TattooManagement/images/stats/stats_duracion_semana.jpg" alt="porcentaje duracion semana" /></p>
                                <p>Total horas invertidas: <%= ((PersistenceInterface)application.getAttribute("persistence")).getDuracionesTotales(request.getParameter("user").toString()) %></p>
                                <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                    </div>
                    <div id="stats_estudio" style='display:none;'>  
                        <p>
                            <span class="header" >Estad&iacute;sticas de estudio</span>
                        </p>
                        <p><a id="user_stats_btn">Estadísticas de usuario</a></p> 
                        <div id="ncitas_mes_estudio">
                            <p><a id="semana_stats_btn_4">Estadísticas de la semana</a></p>
                            <% GeneradorDeEstadiaticasDeCitas porcentajeMesEstudio = new GeneradorDeEstadiaticasDeCitas();
                            int citasTotales = porcentajeMesEstudio.totalCitasMesEstudio((PersistenceInterface) application.getAttribute("persistence"), request.getParameter("user").toString());
                                if (porcentajeMesEstudio.porcentajeCitasMesEstudio(((PersistenceInterface) application.getAttribute("persistence")), 
                                    "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_citas_mes_estudio.jpg",
                                    //application.getRealPath("/images/stats/") + "\\stats_citas_mes_estudio.jpg", 
                                    request.getParameter("user").toString()) == true) {%>
                                    <p><span class="headerTable" style="text-align: center" >Citas por mes</span></p>
                                    <p><img src="/TattooManagement/images/stats/stats_citas_mes_estudio.jpg" alt="porcentaje citas mes" /></p>
                                    <p>Citas registradas totales: <%= citasTotales %></p>
                                    <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="ncitas_semana_estudio" style='display:none;'>
                            <p><a id="mes_stats_btn_4">Estadísticas del mes</a></p>
                            <%  if (porcentajeMesEstudio.porcentajeCitasSemanaEstudio(((PersistenceInterface) application.getAttribute("persistence")), 
                                    "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_citas_semana_estudio.jpg",
                                    //application.getRealPath("/images/stats/") + "\\stats_citas_mes_estudio.jpg", 
                                    request.getParameter("user").toString()) == true) {%>
                                    <p><span class="headerTable" style="text-align: center" >Citas por día de la semana</span></p>
                                    <p><img src="/TattooManagement/images/stats/stats_citas_semana_estudio.jpg" alt="porcentaje citas semana" /></p>
                                    <p>Citas registradas totales: <%= citasTotales %></p>
                                    <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="dinero_mes_estudio">
                            <p><a id="semana_stats_btn_5">Estadísticas de la semana</a></p>
                            <% int dinerosTotales = porcentajeMesEstudio.totalDinerosMesEstudio((PersistenceInterface) application.getAttribute("persistence"), request.getParameter("user").toString());
                            if (porcentajeMesEstudio.porcentajeDinerosMesEstudio(((PersistenceInterface) application.getAttribute("persistence")), 
                                "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_dinero_mes_estudio.jpg",
                                //application.getRealPath("/images/stats/") + "\\stats_dineros_mes_estudio.jpg", 
                                request.getParameter("user").toString()) == true) {%>
                                <p><span class="headerTable" style="text-align: center" >Ganancias por mes</span></p>
                                <p><img src="/TattooManagement/images/stats/stats_dinero_mes_estudio.jpg" alt="porcentaje ganancias mes" /></p>
                                <p>Ganancias totales: <%= dinerosTotales %> €</p>
                                <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="dinero_semana_estudio" style='display:none;'>
                            <p><a id="mes_stats_btn_5">Estadísticas del mes</a></p>
                            <% if (porcentajeMesEstudio.porcentajeDinerosSemanaEstudio(((PersistenceInterface) application.getAttribute("persistence")), 
                                "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_dinero_semana_estudio.jpg",
                                //application.getRealPath("/images/stats/") + "\\stats_dineros_mes_estudio.jpg", 
                                request.getParameter("user").toString()) == true) {%>
                                <p><span class="headerTable" style="text-align: center" >Ganancias por día de la semana</span></p>
                                <p><img src="/TattooManagement/images/stats/stats_dinero_semana_estudio.jpg" alt="porcentaje ganancias semana" /></p>
                                <p>Ganancias totales: <%= dinerosTotales %> €</p>
                                <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="duracion_mes_estudio">
                            <p><a id="semana_stats_btn_6">Estadísticas de la semana</a></p>
                            <% int duracionesTotales = porcentajeMesEstudio.totalDuracionesMesEstudio((PersistenceInterface) application.getAttribute("persistence"), request.getParameter("user").toString());
                            if (porcentajeMesEstudio.porcentajeDuracionesMesEstudio(((PersistenceInterface) application.getAttribute("persistence")), 
                                "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_duracion_mes_estudio.jpg",
                                //application.getRealPath("/images/stats/") + "\\stats_dineros_mes_estudio.jpg", 
                                request.getParameter("user").toString()) == true) {%>
                                <p><span class="headerTable" style="text-align: center" >Duración de citas por mes</span></p>
                                <p><img src="/TattooManagement/images/stats/stats_duracion_mes_estudio.jpg" alt="porcentaje duracion mes" /></p>
                                <p>Total horas invertidas: <%= duracionesTotales %> </p>
                                <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                        <div id="duracion_semana_estudio" style='display:none;'>
                            <p><a id="mes_stats_btn_6">Estadísticas del mes</a></p>
                            <% if (porcentajeMesEstudio.porcentajeDuracionesSemanaEstudio(((PersistenceInterface) application.getAttribute("persistence")), 
                                "/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/stats/stats_duracion_semana_estudio.jpg",
                                //application.getRealPath("/images/stats/") + "\\stats_dineros_mes_estudio.jpg", 
                                request.getParameter("user").toString()) == true) {%>
                                <p><span class="headerTable" style="text-align: center" >Duración de citas por día de la semana</span></p>
                                <p><img src="/TattooManagement/images/stats/stats_duracion_semana_estudio.jpg" alt="porcentaje duracion semana" /></p>
                                <p>Total horas invertidas: <%= duracionesTotales %> </p>
                                <br />
                            <% } else {%>
                                <p>Error generando el gr&aacute;fico</p>
                            <% }%>
                        </div>
                    </div>
                    <!-- Crea las esquinas redondeadas abajo -->
                    <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>
                </div>
            </div>
            <!-- Pie de pagina -->
            <%@include file="/WEB-INF/include/footer.jsp" %>
        </div>
    </body>
</html>

<%! String menuInicio = ""; %>
<%! String menuCitas = ""; %>
<%! String menuLogin = ""; %>
<%! String menuPreferencias = "class=\"active\""; %>
<%! String menuAbout = ""; %>

