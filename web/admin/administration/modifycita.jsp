<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.util.Date"%>
<%@page import="java.util.ArrayList"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="control.Tools"%>
<%@page import="modelo.Cita"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%if (validar(request, session) == false) {
        response.sendError(404);
        return;
}%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="icon" href="images/icons/icon.png"> </link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Modificar Cita</title>

        <script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
        <script type="text/javascript" src="/scripts/vanadium.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/validacion.css" media="screen, tv, projection" />

        <script type="text/javascript" src="/scripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript" src="/scripts/scripts.js"></script>
        
        <script type="text/javascript" src="/scripts/jquery.js"></script>
        <script type="text/javascript" src="/scripts/jquery.datetimepicker.full.min.js"></script>
        <script type="text/javascript" src="/scripts/jquery.datetimepicker.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/jquery.datetimepicker.css" media="screen, tv, projection" />

        <link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
    </head>

    <body onload="loadEditor();">
        <!-- Contenedor principal-->
        <div id="siteBox">

            <!--Cabecera-->
            <%@include file="/WEB-INF/include/header.jsp" %>

            <!-- Contenido de la pagina -->
            <div id="content">

                <!-- Menu Izquiero : side bar links/news/search/etc. -->
                <%@include file="/WEB-INF/include/menuAdministracion.jsp" %>

                <!-- Contenido de la columna derecha -->
                <div id="contentRight">
                    <%
                        String nombreContacto = "";
                        String precio = "";
                        String telefonoContacto = "";
                        String fechaHora = "";
                        String detalles = "";
                        String duracion = "";
                        int fianza=0;
                        int recordatorio = 0;
                        String codigo = "";
                        boolean encontrado = true;
                        Cita cita = null;

                        PersistenceInterface persistencia = (PersistenceInterface) application.getAttribute("persistence");
                        cita = persistencia.getCita(request.getParameter("cita"));
                        if (cita != null) {
                            encontrado = true;
                        } else {
                            encontrado = false;
                        }

                        if (cita != null) {
                            nombreContacto = "value=\"" + cita.getNombreContacto() + "\"";
                            precio = "value=\"" + cita.getPrecioInt() + "\"";
                            telefonoContacto = "value=\"" + cita.getTelefonoContacto() + "\"";
                            fechaHora = cita.getFechaString()+" "+cita.getHoraString();
                            detalles = cita.getDetalles();
                            duracion = "value=\"" + cita.getDuracion() + "\"";
                            fianza = cita.getFianza();
                            codigo = cita.getCodigo();
                            recordatorio = cita.getRecordatorio();
                        }

                    %>

                    <% if (encontrado == true) {%>
                        <p>
                            <span class="header">Modificar Cita</span>
                        </p>
                        <form name="modifycita" method="post" action="/admin/administration/editcita" enctype="multipart/form-data">
                            <input type="hidden" name="codigo" value="<%= codigo%>" />
                            <b>Identificador Usuario</b> <br />
                            <input type="text" name="idUsuario" maxlength="70" size="50" value="<%=session.getAttribute("MailUsuario")%>" readonly/><br /><br />
                            <b>Nombre Contacto</b> <br />
                            <input type="text" name="nombreContacto" maxlength="70" size="50" class=":alpha :required :only_on_blur" <%= nombreContacto%>/><br /><br />
                            <b>Precio</b> <br />
                            <input type="text" name="precio" maxlength="10" size="13" class=":number :required :only_on_blur" <%= precio%> /><br /><br />
                            <b>Duración</b> <input type="text" name="duracionAntigua" maxlength="2" size="8" class=":number :required :only_on_blur" <%= duracion%>  style="visibility:hidden" readonly/><br />
                            <input type="text" name="duracion" maxlength="2" size="8" class=":number :required :only_on_blur" <%= duracion%> /><br /><br />
                            <b>Teléfono Contacto</b> <br />
                            <input type="text" name="telefonoContacto" maxlength="9" size="9" class=":digits :required :only_on_blur" <%= telefonoContacto%> /><br /><br />
                            <b>Fianza</b> <br />
                            <% if (fianza==1) { %>
                                <input type="radio" name="fianza" value="1" checked="checked"/> SI
                                <input type="radio" name="fianza" value="0" /> NO <br /><br />
                            <%} else if (fianza==0) { %>
                                <input type="radio" name="fianza" value="1" /> SI
                                <input type="radio" name="fianza" value="0" checked="checked"/> NO <br /><br />
                            <%}%>  
                            <b>Recordatorio a cliente?</b> <br />
                            <% if (recordatorio==1) { %>
                                <input type="radio" name="recordatorio" value="1" checked="checked"/> SI
                                <input type="radio" name="recordatorio" value="0" /> NO <br /><br />
                            <%} else if (recordatorio==0) { %>
                                <input type="radio" name="recordatorio" value="1" /> SI
                                <input type="radio" name="recordatorio" value="0" checked="checked"/> NO <br /><br />
                            <%}%>
                            <b>Escoja una foto de producto</b>&nbsp;(Opcional)<br />
                            <input type="file" name="foto" /><br /><br /> 
                            <!-- <b>Antigua fecha </b> <br /> -->
                            <b>Fecha: <%=fechaHora%></b> <br />
                            <!-- <div id="prueba"> PRUEBA </div> -->
                            <input id="datetimepicker" name="fechaHora" type="text" value="<%=fechaHora%>" /><br />
                            <script>
                                <%  //OBTENEMOS TODAS LAS FECHAS DE LA BD 
                                    List <Date> dates=new ArrayList<Date>();
                                    dates = ((PersistenceInterface) application.getAttribute("persistence")).getFechasAsociadas(session.getAttribute("MailUsuario").toString()); 
                                %>
                                var tamano='<%=dates.size()%>';
                                var SelectedDates=[tamano];
                                <%int i;%>
                                <% SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm"); 
                                SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy/MM/dd"); 
                                for(i=0; i<dates.size(); i++) {%>
                                        SelectedDates['<%=i%>'] = new Date('<%=formatter2.format(dates.get(i))%>');   
                                <%} %>
                                jQuery('#datetimepicker').datetimepicker({
                                    formatDate: "Y/m/d", 
                                    minDate:0,
                                    onGenerate:function( ct ){
                                        for (var j=0; j<SelectedDates.length; j++){
                                            $('[data-year="'+SelectedDates[j].getFullYear().toString()+'"][data-month="'+SelectedDates[j].getMonth().toString()+'"][data-date="'+SelectedDates[j].getDate()+'"]').addClass('xdsoft_highlighted_default');
                                        };
                                        jQuery(this).find('.xdsoft_date.xdsoft_weekend')
                                          .addClass('xdsoft_disabled');
                                    }
                                });
                                $.datetimepicker.setLocale('es');
                            </script>
                            <input type="text" name="fechaAntigua" type="text" value="<%=fechaHora%>" style="visibility:hidden" readonly/><br />
                            <b>Detalles</b>&nbsp;(Opcional) <br />
                            <textarea name="detail" cols="60" rows="15" class=":only_on_blur"><%= detalles.replace("<br />", "\n")%></textarea><br /><br />
                            <input type="submit" name="sendCita" value="Enviar datos" />
                            <input type="reset" name="limpiar" value="Borrar datos del formulario" />
                        </form>
                    <% } else {%>
                        <p>
                            <span class="header">Cita no encontrada</span><br />
                            Ha ocurrido un error editando la cita. La cita no ha sido encontrado<br /><br />
                            <a href="/admin/administration/citas_administration.jsp">Administraci&oacute;n de citas</a>
                        </p>
                    <% }%>
                    <!-- Crea las esquinas redondeadas abajo -->
                    <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom" />
                </div>
            </div>
            <!-- Pie de pagina -->
            <%@include file="/WEB-INF/include/footer.jsp" %>
        </div>
    </body>
</html>

<%--Funcion para validar la entrada en esta jsp con los parametros necesarios--%>
<%! private boolean validar(HttpServletRequest request, HttpSession sesion) {
        if (request.getParameterMap().size() >= 1 && request.getParameter("cita") != null) {
            return Tools.validateUUID(request.getParameter("cita")); 
        } else {
            return false;
        }
    }%>

<%! String menuInicio = "";%>
<%! String menuCitas = "";%>
<%! String menuLogin = "";%>
<%! String menuPreferencias = "class=\"active\"";%>
<%! String menuAbout = "";%>