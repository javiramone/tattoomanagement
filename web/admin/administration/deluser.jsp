<%@page import="org.owasp.esapi.errors.ValidationException"%>
<%@page import="org.owasp.esapi.errors.IntrusionException"%>
<%@page import="org.owasp.esapi.errors.IntrusionException"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="control.Tools"%>
<%@page import="modelo.Usuario"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<%if (validar (request, session) == false){
    request.setAttribute("error", "");
    response.sendError(404);
    return;
} %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon" href="images/icons/icon.png"> </link>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Borrar cita</title>

<link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
</head>

<body>
<!-- Contenedor principal-->
<div id="siteBox">

	<!--Cabecera-->
    <%@include file="/WEB-INF/include/header.jsp" %>


  <!-- Contenido de la pagina -->
  <div id="content">

  <!-- Menu Izquiero : side bar links/news/search/etc. -->
  <%@include file="/WEB-INF/include/menuAdministracion.jsp" %>

    <!-- Contenido de la columna derecha -->
    <div id="contentRight">
        <% PersistenceInterface persistence = (PersistenceInterface)application.getAttribute("persistence");
        Usuario usuario = persistence.getUser(request.getParameter("user"));
        if (usuario != null){ %>
        <p>
            <span class="header">Borrar usuario <%= usuario.getNombre() %></span>
        </p>
        <ul>
                <li> <b>Nombre: </b><%= usuario.getNombre() %> </li>
                <li> <b>Email: </b> <%= usuario.getMail() %> </li>
                <li> <b>Estudio: </b> <%= usuario.getEstudio()%> </li>
        </ul><br /><br />
        <p>¿Esta seguro de que desea borrar el usuario?</p>
        <a style="margin-left: 2em" href="/admin/administration/user_administration.jsp">No eliminar</a>
        <a style="margin-left: 15em" href="/admin/administration/deluser?user=<%= usuario.getMail() %>">Eliminar usuario</a>
        <% } else{ %>
        <p>
        <span class="header">Usuario no encontrado</span><br />
        Ha ocurrido un error en la operaci&oacute;n. El usuario no ha sido encontrado<br /><br />
        <a href="/admin/administration/user_administration.jsp">Administraci&oacute;n de usuarios</a>
        </p>
        <% } %>

      <!-- Crea las esquinas redondeadas abajo -->
      <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>

    </div>
</div>

<!-- Pie de pagina -->
<%@include file="/WEB-INF/include/footer.jsp" %>

</div>

</body>
</html>

<%! String menuInicio = ""; %>
<%! String menuCitas = ""; %>
<%! String menuLogin = ""; %>
<%! String menuPreferencias = "class=\"active\""; %>
<%! String menuAbout = ""; %>

<%--Funcion para validar la entrada en esta jsp con los parametros necesarios--%>
<%! private boolean validar (HttpServletRequest request, HttpSession sesion)throws IntrusionException, ValidationException{
    if (request.getParameterMap().size()>=1 && request.getParameter("user")!= null ){
        return Tools.isEmailValidate(request.getParameter("user"));
    }else return false;
} %>