<%@page import="persistencia.PersistenceInterface"%>
<%@page import="control.Tools"%>
<%@page import="modelo.Cita"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<%if (validar (request, session) == false){
    request.setAttribute("error", "");
    response.sendError(404);
    return;
} %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon" href="images/icons/icon.png"> </link>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

<title>Borrar cita</title>

<link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
</head>

<body>
<!-- Contenedor principal-->
<div id="siteBox">
    <!--Cabecera-->
    <%@include file="/WEB-INF/include/header.jsp" %>
  
    <!-- Contenido de la pagina -->
    <div id="content">

    <!-- Menu Izquiero : side bar links/news/search/etc. -->
    <%@include file="/WEB-INF/include/menuAdministracion.jsp" %>

    <!-- Contenido de la columna derecha -->
    <div id="contentRight">
        <% PersistenceInterface persistence = (PersistenceInterface)application.getAttribute("persistence");
        Cita cita = persistence.getCita(request.getParameter("cita"));
        if (cita != null){ %>
        <p>
            <span class="header">Borrar cita: <%= cita.getCodigo() %></span>
        </p>
        <ul>
                <li> <b>Precio: </b><%= Tools.roundDouble(cita.getPrecio())%> &euro; </li>
                <li> <b>Duración: </b> <%= cita.getDuracion()%> horas</li>
                <li> <b>Nombre de contacto: </b> <%= cita.getNombreContacto()%> </li>
                <li> <b>Teléfono contacto: </b> <%= cita.getTelefonoContacto()%> </li>
                <li> <b>Fecha: </b> <%= cita.getFechaString()%> </li>
                <li> <b>Hora: </b> <%= cita.getHoraString()%> </li>
        </ul><br /><br />
        <p>¿Esta seguro de que desea borrar la cita?</p>
        <a style="margin-left: 2em" href="/citas/citas.jsp">No eliminar</a>
        <a style="margin-left: 15em" href="/admin/administration/delcita?cita=<%= cita.getCodigo() %>">Eliminar cita</a>
        <% } else{ %>
        <p>
        <span class="header">Cita no encontrada</span><br />
        Ha ocurrido un error en la operaci&oacute;n. La cita no ha sido encontradoa<br /><br />
        <a href="/citas/citas.jsp">Administraci&oacute;n de citas</a>
        </p>
        <% } %>

      <!-- Crea las esquinas redondeadas abajo -->
      <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>

    </div>
</div>

<!-- Pie de pagina -->
<%@include file="/WEB-INF/include/footer.jsp" %>

</div>

</body>
</html>

<%! String menuInicio = ""; %>
<%! String menuCitas = ""; %>
<%! String menuLogin = ""; %>
<%! String menuPreferencias = "class=\"active\""; %>
<%! String menuAbout = ""; %>

<%--Funcion para validar la entrada en esta jsp con los parametros necesarios--%>
<%! private boolean validar (HttpServletRequest request, HttpSession sesion){
    if (request.getParameterMap().size()>=1 && request.getParameter("cita")!= null ){
        return Tools.validateUUID(request.getParameter("cita"));
    }else return false;
} %>