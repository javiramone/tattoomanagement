<%@page import="control.Tools"%>
<%@page import="modelo.Cita"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="java.util.Map"%>
<%@ page contentType="text/html; charset=UTF-8" language="java"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="icon" href="images/icons/icon.png"> </link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Gesti&oacute;n de productos</title>

        <script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
        <script type="text/javascript" src="/scripts/vanadium.js"></script>
        <script type="text/javascript" src="/scripts/basic.js"></script>
        <script type="text/javascript" src="/scripts/jquery.simplemodal.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/validacion.css" media="screen, tv, projection" />
        <link rel="stylesheet" type="text/css" href="/css/basic.css" media="screen, tv, projection" />
        <link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
    </head>

    <body>
        <!-- Contenedor principal-->
        <div id="siteBox">

            <!--Cabecera-->
            <%@include file="/WEB-INF/include/header.jsp" %>


            <!-- Contenido de la pagina -->
            <div id="content">

                <!-- Menu Izquiero -->
                <%@include file="/WEB-INF/include/menuAdministracion.jsp" %>

                <!-- Contenido de la columna derecha -->
                <div id="contentRight">

                    <p>
                        <span class="header">Administraci&oacute;n de citas</span><br /><br />
                        <span class="subHeader">Funciones disponibles</span>
                    </p>
                    <ul>
                        <li><a href="/admin/administration/addcita.jsp">A&ntilde;adir Cita</a></li> 
                        <li>
                            <form name="busquedaCitas" method="post" action="/citas/search_citas">
                                <input type="hidden" name="redirect" value="/admin/administration/citas_administration.jsp" />
                                <input name="term" type="text" class=":alpha :required :only_on_blur" />
                                <select name="campo">
                                    <option value="name">Nombre Contacto</option>
                                    <option value="duracion">Duración</option>
                                    <option value="detail">Detalles</option>
                                </select>
                                <input name="search" value="Buscar citas" type="submit" />
                            </form> 
                        </li>
                    </ul>
                    <br />
                    <% Map<String, Cita> citas = null;
                        if (request.getAttribute("resultadosBusqueda") == null) {
                            citas = ((PersistenceInterface) application.getAttribute("persistence")).getCitas();
                        } else {
                            citas = (Map<String, Cita>) request.getAttribute("resultadosBusqueda");
                        }
                        if (citas != null && citas.size() != 0) {%>
                    <p>
                        <span class="subHeader">Listado de citas</span>
                    </p>
                        
                        <br />

                        <table border="0" align="center" width="90%">
                            <tr class="headerTable"><td>Nombre Contacto</td><td>Duración</td><td>Detalles</td><td>&nbsp;</td></tr>
                            <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <% for (Cita cita : citas.values()) {%>
                            <tr class="contentTable">
                                <!-- <td><a href="/citas/viewcita.jsp?prod=<%= cita.getCodigo()%>"><%= cita.getNombreContacto()%></a></td> -->
                                <td><%= Tools.roundDouble(cita.getDuracion())%> &euro;</td>
                                <td><%= cita.getDetalles()%></td>
                                <td>
                                     <a href="#" id="basic-modal">
                                        <img class="iconsMenu, basic" title="Editar cita" alt="Editar cita" src="/images/icons/editCita.png"/>
                                    </a> &nbsp;&nbsp;
                                    <a href="/admin/administration/delcita.jsp?cita=<%= cita.getCodigo()%>">
                                        <img title="Borrar cita" alt="Borrar cita" src="/images/icons/deleteCita.png"/>
                                    </a>
                                        <!-- modal content -->
                                                <div id="basic-modal-content">
                                                        <h3>OPCIONES</h3>
                                                        <p>Seleccione que desea hacer:</p>
                                                        <p><a href="/admin/administration/modifyfechacita.jsp?cita=<%= cita.getCodigo()%>" id="basic-modal"> Modificar automáticamente la fecha y hora de la cita por la siguiente disponible</a></p>
                                                        <p><a href="/admin/administration/modifycita.jsp?cita=<%= cita.getCodigo()%>" id="basic-modal"> Modificar más datos de la cita</a></p>
                                                </div>
                                </td>
                            </tr>
                            <% }%>
                        </table>
                    <% } else {%>
                    <p>No se han encontrado citas</p>
                    <% }%> 

                    <!-- Crea las esquinas redondeadas abajo -->
                    <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>    
                </div>
            </div>

            <!-- Pie de pagina -->
            <%@include file="/WEB-INF/include/footer.jsp" %>

        </div>

    </body>
</html>

<%! String menuInicio = "";%>
<%! String menuCitas = "";%>
<%! String menuLogin = "";%>
<%! String menuPreferencias = "class=\"active\"";%>
<%! String menuAbout = "";%>

