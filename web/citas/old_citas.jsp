<%@page import="control.Tools"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="modelo.Cita"%>
<%@page import="modelo.Usuario"%>
<%@page import="java.util.Map"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>

<% boolean iniciado = false;
   PersistenceInterface persistencia = (PersistenceInterface) application.getAttribute("persistence");
   if (session.getAttribute("auth") != null && (Boolean) session.getAttribute("auth") == true && session.getAttribute("usuario") != null){ 
       Usuario user = persistencia.getUser((String)session.getAttribute("usuario"));
       if (user != null){
            iniciado = true;
            //long fechaSesion = session.getCreationTime();
       }else{
            response.sendRedirect("/logout");
       }
} %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon" href="images/icons/icon.png"> </link>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>TattooManagement</title>

    <script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
    <script type="text/javascript" src="/scripts/vanadium.js"></script>
    <script type="text/javascript" src="/scripts/basic.js"></script>
    <script type="text/javascript" src="/scripts/jquery.simplemodal.js"></script>
    <link rel="stylesheet" type="text/css" href="/css/validacion.css" media="screen, tv, projection" />
    <link rel="stylesheet" type="text/css" href="/css/basic.css" media="screen, tv, projection" />
    <link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
</head>

<body>
<!-- Contenedor principal-->
<div id="siteBox">
    <!--Cabecera-->
    <%@include file="/WEB-INF/include/header.jsp" %>

    <!-- Contenido de la pagina -->
    <div id="content">

        <!-- Menu Izquiero -->
        <%@include file="/WEB-INF/include/menu.jsp" %>

        <!-- Contenido de la columna derecha -->
        <div id="contentRight">

            <%@include file="/WEB-INF/include/resultados.jsp" %>

            <p>
                <span class="header">Citas</span>
            </p>
            <% if (iniciado == true){ %>
                <form name="busquedaCitas" method="post" action="/citas/search_old_citas">
                    <input type="hidden" name="idUsuario" value="<%=session.getAttribute("MailUsuario")%>" />
                    <input type="hidden" name="redirect" value="/citas/citas.jsp" />
                    <input name="term" type="text" class=":required :only_on_blur"/>
                    <select name="campo">
                        <option value="NombreContacto">Nombre</option>
                        <option value="Duracion">Duracion</option>
                        <option value="Detalles">Detalles</option>
                        <option value="TelefonoContacto">Teléfono Contacto</option>
                    </select>
                    <input name="search" value="Buscar citas" type="submit" />
                </form><br />
        
                <% if (session.getAttribute("auth") != null && (Boolean)session.getAttribute("auth") == true){ %>
                    <a href="/admin/administration/addcita.jsp" title="Administraci&oacute;n de citas" class="menuItem">Añadir citas</a>
                    <a href="/citas/citas.jsp" title="Administraci&oacute;n de citas" class="menuItem">Próximas Citas</a>    
                    <a href="/citas/all_citas.jsp" title="Administraci&oacute;n de citas" class="menuItem">Todas las Citas</a>
                <%}%>
                <% Map <String, Cita> citas = null;
                    if (request.getAttribute("resultadosBusqueda") == null){
                        citas = ((PersistenceInterface) application.getAttribute("persistence")).getOldCitasAsociadas(session.getAttribute("MailUsuario").toString());
                    }else{
                        citas = (Map <String, Cita>)request.getAttribute("resultadosBusqueda");
                    }
                    if (citas != null && citas.size() != 0){%>
                        <table border="0" align="center" width="90%">
                            <tr class="headerTable"><td> </td><td>Nombre Contacto</td><td>Precio</td><td>Duración</td><td>Telefono Contacto</td><td>Fecha</td></tr>
                            <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                            <% for (Cita cita : citas.values()){ %>
                                <tr class="contentTable">
                                <td> 
                                    <a href="/admin/administration/delcita.jsp?cita=<%= cita.getCodigo()%>">
                                        <img title="Borrar cita" alt="Borrar cita" src="/images/icons/deleteCita.png"/>
                                    </a>
                                    <a href="#" id="basic-modal">
                                    <img class="iconsMenu, basic" title="Modificar cita" alt="Modificar cita" src="/images/icons/editCita.png"/>
                                    </a>
                                    <!-- modal content -->
                                    <div id="basic-modal-content">
                                        <h3>OPCIONES</h3>
                                            <p>Seleccione que desea hacer:</p>
                                            <p><a href="/admin/administration/modifyfechacita.jsp?cita=<%= cita.getCodigo()%>" id="basic-modal"> Modificar automáticamente la fecha y hora de la cita por la siguiente disponible</a></p>
                                            <p><a href="/admin/administration/modifycita.jsp?cita=<%= cita.getCodigo()%>" id="basic-modal"> Modificar más datos de la cita</a></p>
                                    </div>
                                </td>
                                <td>
                                    <a href="/citas/viewcita.jsp?cita=<%= cita.getCodigo() %>"><%= cita.getNombreContacto() %>
                                    </a>
                                </td>
                                <td><%= Tools.roundDouble(cita.getPrecio()) %> &euro;</td>
                                <td><%= Tools.roundDouble(cita.getDuracion())%></td>
                                <td><%= cita.getTelefonoContacto() %></td>
                                <td><%= cita.getFechaString()%> <%= cita.getHoraString()%></td>
                                <td></td>
                                </tr>
                            <% } %>
                        </table>
                    <% } else{ %>
                        <p>No se han encontrado citas</p>
                    <% } %>
            <% } else { %>
                <p>No se han encontrado citas. Para consultar tus citas, inicia sesión <a href="/login.jsp">aquí</a></p>
            <% } %>
      <!-- Crea las esquinas redondeadas abajo -->
      <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>

    </div>
</div>

<!-- Pie de pagina -->
<%@include file="/WEB-INF/include/footer.jsp" %>

</div>

</body>
</html>

<%! String menuInicio = ""; %>
<%! String menuCitas = "class=\"active\""; %>
<%! String menuLogin = ""; %>
<%! String menuPreferencias = ""; %>
<%! String menuAbout = ""; %>