<%@page import="modelo.Cita"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="control.Tools"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<% if (validateEntry(request) == false) {
        response.sendError(404);
        return;
    }
%>
<% PersistenceInterface persistencia = (PersistenceInterface) application.getAttribute("persistence");
    Cita cita = persistencia.getCita(request.getParameter("cita"));
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="icon" href="images/icons/icon.png"> </link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <% if (cita == null) { %>
        <title>Cita no encontrada</title>
        <% } else {%>
        <title> ADMINISTRACIÓN DE CITAS</title>
        <% }%>
        
        <script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
        <script type="text/javascript" src="/scripts/vanadium.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/validacion.css" media="screen, tv, projection" />
        
        <script type="text/javascript" src="/scripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript" src="/scripts/scripts.js"></script>
        
        <link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
    </head>

        <body onload="loadEditor();">
        <!-- Contenedor principal-->
        <div id="siteBox">

            <!--Cabecera-->
            <%@include file="/WEB-INF/include/header.jsp" %>

            <!-- Contenido de la pagina -->
            <div id="content">

                <!-- Menu Izquiero -->
                <%@include file="/WEB-INF/include/menu.jsp" %>

                <!-- Contenido de la columna derecha -->
                <div id="contentRight">

                    <%-- Resultados de la operaci&oacute;n --%>
                    <%@include file="/WEB-INF/include/resultados.jsp" %>

                    <% if (cita == null) {%>
                    <p>
                        <span class="header">Cita no encontrada</span>
                    </p>
                    <p>
                        La cita seleccionada no ha podido ser encontrada. Posiblemente esta petici&oacute;n fue alterada <br />
                        Puede volver al <a href="/citas/citas.jsp">listado de citas.
                        <%request.getParameter("cita");%></a>
                    </p>
                    <% } else {%>
                    <p>
                        <span class="header" >Hoja de Cita: <span class="headerComplement" ><%= cita.getCodigo()%></span></span>
                        <br />
                        <%if (Tools.existeElFichero("/opt/shared/glassfish/domains/domain1/applications/TattooManagement/images/citas/"+ cita.getCodigo() + ".jpg") == true){ %>
                        <img class="marco" src="/TattooManagement/images/citas/<%= cita.getCodigo()+".jpg"%>" 
                             alt="/TattooManagement/images/citas/<%= cita.getCodigo()+".jpg"%>" height="400" width="450" align="right"/> <br /> 
                        <% } %>
                            
                        <ul>
                            <TABLE BORDER=1>
                                <td>
                                    <li> <b>Precio: </b><%= Tools.roundDouble(cita.getPrecio())%> &euro; </li>
                                    <li> <b>Duración: </b> <%= cita.getDuracion()%> horas</li>
                                    <li> <b>Nombre de contacto: </b> <%= cita.getNombreContacto()%> </li>
                                    <li> <b>Teléfono contacto: </b> <%= cita.getTelefonoContacto()%> </li>
                                    <li> <b>Fecha: </b> <%= cita.getFechaString()%> </li>
                                    <li> <b>Hora: </b> <%= cita.getHoraString()%> </li>
                                    <li> <b>Fianza: </b> <%= cita.getFianzaString()%> </li>
                                    <li> <b> Detalles de la cita: </b> <font size="3"> <%= cita.getDetalles()%> </font>  </li>
                                </td>
                            </TABLE>
                                <br /> <br /> <br /> <br /> <br />
                        </ul> 
                    </p>                                       
                    <% }%>
                    <a class="menuItem" href="/admin/administration/modifycita.jsp?cita=<%= cita.getCodigo() %>"> Modificar Cita </a>
                    <a class="menuItem" href="/admin/administration/delcita.jsp?cita=<%= cita.getCodigo()%>"> Eliminar Cita </a>
                    <br /> 
                    <!-- Crea las esquinas redondeadas abajo -->
                    <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>

                </div>
            </div>
            <!-- Pie de pagina -->
            <%@include file="/WEB-INF/include/footer.jsp" %>
        </div>
    </body>
</html>

<%! private boolean validateEntry(HttpServletRequest request) {
        if (request.getParameterMap().size() >= 1 && request.getParameter("cita") != null) {
            return Tools.validateUUID(request.getParameter("cita"));
        } else {
            return false;
        }
    }%>

<%! String menuInicio = "";%>
<%! String menuCitas = "class=\"active\"";%>
<%! String menuLogin = "";%>
<%! String menuPreferencias = "";%>
<%! String menuAbout = "";%>
