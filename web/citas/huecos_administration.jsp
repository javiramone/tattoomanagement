<%@page import="java.util.Calendar"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="modelo.Usuario"%>
<%@page import="modelo.Cita"%>
<%@page import="java.util.Date" %>
<%@page import="java.text.DateFormat"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="java.util.Map"%>

<% boolean iniciado = false;
    boolean seleccionado = false;
   PersistenceInterface persistencia = (PersistenceInterface) application.getAttribute("persistence");
   if (session.getAttribute("auth") != null && (Boolean) session.getAttribute("auth") == true && session.getAttribute("usuario") != null){ 
       Usuario user = persistencia.getUser((String)session.getAttribute("usuario"));
       if (user != null){
            iniciado = true;
       }else{
            response.sendRedirect("/logout");
       }
} %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="icon" href="images/icons/icon.png"> </link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>TattooManagement</title>
        <script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
        <script>
            $(document).ready(function(){
                $("#moreResults_1").click(function(){
                    $("#results_1").toggle("slow", function(){
                        $("#results_2").toggle("slow");
                    });     
                });
                $("#moreResults_2").click(function(){
                    $("#results_2").toggle("slow", function(){
                        $("#results_3").toggle("slow");
                    });     
                });
            });
        </script>
    </head>

    <body>
        <!-- Contenedor principal-->
        <div id="siteBox">

            <!--Cabecera-->
            <%@include file="/WEB-INF/include/header.jsp" %>

            <!-- Contenido de la pagina -->
            <div id="content">
                
                <!-- Menu Izquierdo : side bar links/news/search/etc. -->
                <%@include file="/WEB-INF/include/menu.jsp" %>
                
                <!-- Contenido de la columna derecha -->
                <div id="contentRight">

                <span class="header">Búsqueda de disponibilidad</span>
                 Desde aquí podrás buscar de manera fácil y cómoda las fechas disponibles para nuevas citas.
                <% if (iniciado == true){ 
                    DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");%>      
                    <form name="gestdisp" method="post" action="/citas/huecos_administration" >
                        <b>Fecha actual</b> <br />
                            <input type="text" name="fecha" maxlength="70" size="10" value="<%=dateFormat.format(new Date())%>" readonly/><br /><br />
                        <b>Identificador Usuario</b> <br />
                            <input type="text" name="idUsuario" maxlength="70" size="50" value="<%=session.getAttribute("MailUsuario")%>" readonly/><br /><br />
                        <b>Horario</b> <br />
                        <select name="horario">
                            <option value="1">Mañana</option>
                            <option value="2">Tarde</option>
                            <option value="3">Indiferente</option>
                        </select> <br /> <br />
                        <b>Incluir festivos </b>  <br />
                        <input type="radio" name="festivo" value="1">SI </>
                        <input type="radio" name="festivo" value="2" checked>NO <br /> <br />
                        <b>Duración estimada</b> <br />
                        <select name="duracionest">
                            <option value="1">1</option>
                            <option value="2">2</option>
                            <option value="3">3</option>
                            <option value="4">4</option>
                            <option value="5">5</option>
                            <option value="6">6+</option>
                        </select>
                        <br /> <br />
                        <input type="submit" name="searchHuecos" value="Consultar Disponibilidad"/>
                    </form>
                    <%  ArrayList <modelo.Cita> citasDisponibles = null;
                        if (request.getAttribute("resultadosBusquedaHuecos") != null){
                            citasDisponibles = (ArrayList<modelo.Cita>)request.getAttribute("resultadosBusquedaHuecos");
                        }
                        if (citasDisponibles != null && citasDisponibles.size() != 0){ %>
                            <div id="results_1">
                            <table border="0" align="center" width="90%">
                                <tr class="headerTable"><td>Fecha-hora </td><td>Fecha-hora</td><td>&nbsp;</td></tr>
                                <tr class="contentTable" >
                                    <!-- Primeros 14 resultados -->
                                    <% for (int i=0; i<7; i++) {  
                                    Cita citax=citasDisponibles.get(i);
                                    Cita citay=citasDisponibles.get(7+i);
                                     //URL DE ACEPTAR: /addcita.jsp?=YYYY/MM/DD_HH:MM_X%>  
                                    <tr class="contentTable">   
                                        <td><a href="/admin/administration/addcitadatehora.jsp?datecita=<%= citax.getFechaString()%>_<%=citax.getHoraString()%>_<%=citax.getDuracion()%>">
                                                <%= citax.getFechaString()%> <%=citax.getHoraString()%> </a>
                                        </td>
                                        <td><a href="/admin/administration/addcitadatehora.jsp?datecita=<%= citay.getFechaString()%>_<%=citay.getHoraString()%>_<%=citay.getDuracion()%>">
                                                <%= citay.getFechaString()%> <%=citay.getHoraString()%> </a>
                                        </td>
                                    </tr> 
                                    <%}%> 
                                    <a id="moreResults_1"> Más resultados </a>
                                </tr>
                            </table>
                            </div>
                            <div id="results_2" style='display:none;'>
                            <table border="0" align="center" width="90%">
                            <tr class="headerTable"><td>Fecha-hora </td><td>Fecha-hora</td><td>&nbsp;</td></tr>        
                            <tr class="contentTable" >
                                <!-- Resultados del 15 al 28-->
                                <% for (int i=14; i<21; i++) {  
                                Cita citax=citasDisponibles.get(i);
                                Cita citay=citasDisponibles.get(7+i);%>  
                                <tr class="contentTable">   
                                    <td><a href="/admin/administration/addcitadatehora.jsp?datecita=<%= citax.getFechaString()%>_<%=citax.getHoraString()%>_<%=citax.getDuracion()%>">
                                            <%= citax.getFechaString()%> <%=citax.getHoraString()%> </a>
                                    </td>
                                    <td><a href="/admin/administration/addcitadatehora.jsp?datecita=<%= citay.getFechaString()%>_<%=citay.getHoraString()%>_<%=citay.getDuracion()%>">
                                            <%= citay.getFechaString()%> <%=citay.getHoraString()%> </a>
                                    </td>
                                </tr> 
                                <%}%> 
                                <a id="moreResults_2"> Más resultados </a>
                            </tr>
                            </table>
                            </div>
                            <div id="results_3" style='display:none;'>
                            <table border="0" align="center" width="90%">
                            <tr class="headerTable"><td>Fecha-hora </td><td>Fecha-hora</td><td>&nbsp;</td></tr>          
                            <tr class="contentTable">
                                <!-- Resultados del 29 al 42-->
                                <% for (int i=28; i<35; i++) {  
                                Cita citax=citasDisponibles.get(i);
                                Cita citay=citasDisponibles.get(7+i);
                                 //URL DE ACEPTAR: /addcita.jsp?=YYYY/MM/DD_HH:MM_X%>  
                                <tr class="contentTable">   
                                    <td><a href="/admin/administration/addcitadatehora.jsp?datecita=<%= citax.getFechaString()%>_<%=citax.getHoraString()%>_<%=citax.getDuracion()%>">
                                            <%= citax.getFechaString()%> <%=citax.getHoraString()%> </a>
                                    </td>
                                    <td><a href="/admin/administration/addcitadatehora.jsp?datecita=<%= citay.getFechaString()%>_<%=citay.getHoraString()%>_<%=citay.getDuracion()%>">
                                            <%= citay.getFechaString()%> <%=citay.getHoraString()%> </a>
                                    </td>
                                </tr> 
                                <%}%> 
                            </tr>
                            </table>
                            </div>
                        <%}} else { %>
                            <p>No se han encontrado huecos</p>
                        <% } %>

                    <!-- Crea las esquinas redondeadas abajo -->
                    <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>
                </div>
            </div>
            <!-- Pie de pagina -->
            <%@include file="/WEB-INF/include/footer.jsp" %>
        </div>
    </body>
</html>

<%! String menuInicio = "class=\"active\"";%>
<%! String menuCitas = "";%>
<%! String menuLogin = "";%>
<%! String menuPreferencias = "";%>
<%! String menuAbout = "";%>