
<%@page import="org.owasp.esapi.errors.ValidationException"%>
<%@page import="control.Tools"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="modelo.Cita"%>
<%@page import="java.util.Map"%>
<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<% if (validateEntry(request) == false) {
        response.sendError(404);
        return;
    }

    boolean iniciado = false;
    PersistenceInterface persistencia = (PersistenceInterface) application.getAttribute("persistence");
    if (session.getAttribute("auth") != null && (Boolean) session.getAttribute("auth") == true && session.getAttribute("usuario") != null){ 
       Usuario user = persistencia.getUser((String)session.getAttribute("usuario"));
       if (user != null){
            iniciado = true;
            //long fechaSesion = session.getCreationTime();
       }else{
            response.sendRedirect("/logout");
       }
    }
    //Cita cita = persistencia.getCita(request.getParameter("cita"));
    String fechaString = request.getParameter("fecha");
    Date fecha = persistencia.getFecha(request.getParameter("fecha"));
    SimpleDateFormat formatter3 = new SimpleDateFormat("yyyy-MM-dd");
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="icon" href="images/icons/icon.png"> </link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
       
        <title><%= fechaString.replace("-", "/") %></title>
        
        <script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
        <script type="text/javascript" src="/scripts/vanadium.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/validacion.css" media="screen, tv, projection" />
        
        <script type="text/javascript" src="/scripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript" src="/scripts/scripts.js"></script>
        
        <link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
    </head>

        <body onload="loadEditor();">
        <!-- Contenedor principal-->
        <div id="siteBox">

            <!--Cabecera-->
            <%@include file="/WEB-INF/include/header.jsp" %>

            <!-- Contenido de la pagina -->
            <div id="content">

                <!-- Menu Izquiero -->
                <%@include file="/WEB-INF/include/menu.jsp" %>

                <!-- Contenido de la columna derecha -->
                <div id="contentRight">

                    <%-- Resultados de la operaci&oacute;n --%>
                    <%@include file="/WEB-INF/include/resultados.jsp" %>

                    <% if (fecha == null) {%>
                    <p>
                        <span class="header">Fecha no encontrada</span>
                    </p>
                    <p>
                        La fecha seleccionada no ha sido encontrada. <br />
                        Puede volver al <a href="/index.jsp"> la página principal.</a>
                    </p>
                    <% } else if (iniciado == true){ %> 
                            <span class="header" >Citas de: <span class="headerComplement" ><%= fechaString.replace("-", "/") %></span></span>
                            <br />
                            <%-- if (Tools.existeElFichero(application.getRealPath("/images/citas/")+"\\"+cita.getCodigo()+".jpg") == true){ %>
                                <img src="/images/citas/<%= cita.getCodigo()+".jpg"%>" alt="<%=application.getRealPath("/images/citas/"+"\\"+cita.getCodigo()+".jpg") %>" height="400" width="450" align="right"/>
                            <% } --%>
                             <% Map <String, Cita> citas = null; 
                                citas = ((PersistenceInterface) application.getAttribute("persistence")).searchAllCitas("fechaHora", fechaString, session.getAttribute("MailUsuario").toString());                    
                            if (citas != null && citas.size() != 0){%>  
                            
                                <table border="0" align="center" width="90%">
                                    <tr class="headerTable"><td> </td><td>Nombre Contacto</td><td>Precio</td><td>Duración</td><td>Telefono Contacto</td><td>Fecha</td></tr>
                                    <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                    <% for (Cita cita : citas.values()){ %>
                                    <tr class="contentTable">
                                       <td> <a href="/admin/administration/delcita.jsp?cita=<%= cita.getCodigo()%>">
                                                <img title="Borrar cita" alt="Borrar cita" src="/images/icons/deleteCita.png"/>
                                            </a>
                                            <a href="/admin/administration/modifycita.jsp?cita=<%= cita.getCodigo()%>">
                                                <img title="Modificar cita" alt="Modificar cita" src="/images/icons/editCita.png"/>
                                            </a>
                                        </td>
                                        <td>
                                            <a href="/citas/viewcita.jsp?cita=<%= cita.getCodigo() %>"><%= cita.getNombreContacto() %></a></td>
                                        <td><%= Tools.roundDouble(cita.getPrecio()) %> &euro;</td>
                                        <td><%= Tools.roundDouble(cita.getDuracion())%></td>
                                        <td><%= cita.getTelefonoContacto() %></td>
                                        <td><%= cita.getFechaString()%> <%= cita.getHoraString()%></td>
                                        <td>

                                        </td>
                                    </tr>
                                    <% } %>
                                </table>
                        <% } else { %>
                            <p>No se han encontrado citas.</p>
                            
                        <% } %>
                        <a href="/admin/administration/addcitadate.jsp?datecita=<%=fechaString.replace("-", "/")%>" title="Administraci&oacute;n de citas" class="menuItem">Añadir citas</a>
                    <% } else { %>
                        <p>Para consultar tus citas, inicia sesión <a href="/login.jsp">aquí</a></p>
                    <% }%>
                    <!-- Crea las esquinas redondeadas abajo -->
                    <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>
                </div>
            </div>

            <!-- Pie de pagina -->
            <%@include file="/WEB-INF/include/footer.jsp" %>

        </div>

    </body>
</html>

<%! private boolean validateEntry(HttpServletRequest request) {
        if (request.getParameterMap().size() >= 1 && request.getParameter("fecha") != null) {
            try{
                Tools.validateFecha(request.getParameter("fecha"));
                return true;
            }catch (ValidationException ex){
                return false;
            }
        } else {
            return false;
        }
    }%>

<%! String menuInicio = "";%>
<%! String menuCitas = "class=\"active\"";%>
<%! String menuLogin = "";%>
<%! String menuPreferencias = "";%>
<%! String menuAbout = "";%>
