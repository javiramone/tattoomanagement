<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@page import="modelo.Usuario"%>
<%@page import="modelo.Cita"%>
<%@page import="control.Tools"%>
<%@page import="java.util.Date" %>
<%@page import="java.text.DateFormat"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="modelo.Cita"%>

<% boolean iniciado = false;
        if ((PersistenceInterface) application.getAttribute("persistence")!=null){
            PersistenceInterface persistencia = (PersistenceInterface) application.getAttribute("persistence");
            if (request.getSession().getAttribute("auth") != null && (Boolean) request.getSession().getAttribute("auth") == true 
           && request.getSession().getAttribute("usuario") != null){ 
                Usuario user = persistencia.getUser((String)request.getSession().getAttribute("usuario"));
                if (user != null){
                    iniciado = true;
                }else {
                response.sendRedirect("/logout");
                }
            }
        } else {
            response.sendRedirect("/logout");
        }

    %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <link rel="icon" href="images/icons/icon.png"> </link>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>TattooManagement</title>
        <script type="text/javascript" src="/scripts/jquery-1.6.1.js"></script>
        <script type="text/javascript" src="/scripts/basic.js"></script>
        <script type="text/javascript" src="/scripts/jquery.simplemodal.js"></script>
        <link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
        <link rel="stylesheet" type="text/css" href="/css/basic.css" media="screen, tv, projection" />
    </head>

    <body>
        <!-- Contenedor principal-->
        <div id="siteBox">
            
            <div id="cabecera">
                <!--Cabecera-->
                <%@include file="/WEB-INF/include/header.jsp" %>
            </div>
            <!-- Contenido de la pagina -->
            <div id="content">
                <div id="contentLeft">
                    <!-- Menu Izquierdo : side bar links/news/search/etc. -->
                    <%@include file="/WEB-INF/include/menu.jsp" %>

                </div>
                <!-- Contenido de la columna derecha -->
                <div id="contentRight">

                    <p>
                        <div id="textbienvenida"> 
                        <span class="header">Gestión de Citas</span>
                        Desde aquí podrás gestionar de manera fácil, sencilla y visual tus citas como tatuador. Regístrate o inicia sesión <a href="/login.jsp">aquí</a> para comenzar a disfrutar de esta herramienta.
                        </div>
                        <br />
                        <% if (iniciado == true){ %>
                            <a href="/citas/citas.jsp" > 
                                <img id="iconIndex" title="Citas" class="iconMenu" alt="Ir_a_citas" src="/images/icons/citas.jpg" align="left" 
                                      onmouseover="this.src='/images/icons/citas_2.jpg';" onmouseout="this.src='/images/icons/citas.jpg';"/>
                            </a> 
                            <a href="/admin/administration/user_administration.jsp" > 
                                <img id="iconIndex" title="preferencias" class="iconMenu" alt="Ir_a_Preferencias" src="/images/icons/preferencias.jpg" align="left" 
                                    onmouseover="this.src='/images/icons/preferencias_2.jpg';" onmouseout="this.src='/images/icons/preferencias.jpg';"/>                             
                            </a>
                            <a href="/about.jsp" > 
                                <img id="iconIndex" title="about" class="iconMenu" alt="About" src="/images/icons/acerca.jpg"  align="left" 
                                     onmouseover="this.src='/images/icons/acerca_2.jpg';" onmouseout="this.src='/images/icons/acerca.jpg';"/>
                            </a>                            
                            <a href="/logout" >
                                <img id="iconIndex" title="logout" class="iconMenu" alt="salir" src="/images/icons/logout.jpg" align="left" 
                                     onmouseover="this.src='/images/icons/logout_2.jpg';" onmouseout="this.src='/images/icons/logout.jpg';"/>
                            </a> 
                            <br /> <br />
        
                                <span class="header">Próximas Citas</span>
                                <% Map <String, Cita> citasAsociadas = null;
                                citasAsociadas = ((PersistenceInterface) application.getAttribute("persistence")).getCitasAsociadas(session.getAttribute("MailUsuario").toString());
                                if (citasAsociadas != null && citasAsociadas.size() != 0){%>
                                    <table border="0" align="center" width="90%">
                                        <tr class="headerTable"><td> </td><td>Nombre Contacto</td><td class="prescind">Precio</td><td class="prescind">Duración</td><td class="prescind">Telefono Contacto</td><td>Fecha</td></tr>
                                        <tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
                                        <% for (Cita cita : citasAsociadas.values()){ %>
                                        <tr class="contentTable">
                                            <td> <a href="/admin/administration/delcita.jsp?cita=<%= cita.getCodigo()%>">
                                                    <img class="iconsMenu" title="Borrar cita" alt="Borrar cita" src="/images/icons/deleteCita.png"/>
                                                </a>
                                                <a href="#" id="basic-modal">
                                                    <img class="iconsMenu, basic" title="Modificar cita" alt="Modificar cita" src="/images/icons/editCita.png"/>
                                                </a>
                                                <!-- modal content -->
                                                <div id="basic-modal-content">
                                                        <h3>OPCIONES</h3>
                                                        <p>Seleccione que desea hacer:</p>
                                                        <p><a href="/admin/administration/editcitafecha?cita=<%= cita.getCodigo()%>" id="basic-modal"> Modificar automáticamente la fecha y hora de la cita por la siguiente disponible</a></p>
                                                        <p><a href="/admin/administration/modifycita.jsp?cita=<%= cita.getCodigo()%>" id="basic-modal"> Modificar más datos de la cita</a></p>
                                                </div>
                                            </td>
                                            <td>
                                                <a href="/citas/viewcita.jsp?cita=<%= cita.getCodigo() %>"><%= cita.getNombreContacto() %></a></td>
                                            <td class="prescind"><%= Tools.roundDouble(cita.getPrecio()) %> &euro;</td>
                                            <td class="prescind"><%= Tools.roundDouble(cita.getDuracion())%></td>
                                            <td class="prescind"><%= cita.getTelefonoContacto() %></td>
                                            <td><%= cita.getFechaString()%> <%= cita.getHoraString()%></td>
                                            <td>
                                            </td>
                                        </tr>
                                        <% } %>
                                    </table>  

                                <% } else{ %>
                                    <p>No se han encontrado citas </p>
                                <% } %>     
                        <% } %>
                        
                    </p>
                    
                    <!-- Crea las esquinas redondeadas abajo -->
                    <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>

                </div>
            </div>

            <!-- Pie de pagina -->
            <%@include file="/WEB-INF/include/footer.jsp" %>

        </div>

    </body>
</html>

<%! String menuInicio = "class=\"active\"";%>
<%! String menuCitas = "";%>
<%! String menuLogin = "";%>
<%! String menuPreferencias = "";%>
<%! String menuAbout = "";%>