<%@page import="control.Tools"%>
<%@page import="modelo.Usuario"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.util.Iterator"%>
<%@page import="modelo.Cita"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.List"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.Vector"%>
<%@page import="java.util.Date"%>
<%@page import="persistencia.PersistenceInterface"%>
<%@page import="modelo.Usuario"%>

<% boolean iniciadoMenu = false;
List<Date> fechasCalendar = new ArrayList<Date>();
int tamanyo=0;
int ii=0;

    //List<Date> fechasCalendar = new ArrayList<Date>();
        if ((PersistenceInterface) application.getAttribute("persistence")!=null){
            PersistenceInterface persistenciaMenu = (PersistenceInterface) application.getAttribute("persistence");
            if (request.getSession().getAttribute("auth") != null && (Boolean) request.getSession().getAttribute("auth") == true 
           && request.getSession().getAttribute("usuario") != null){ 
                Usuario userMenu = persistenciaMenu.getUser((String)request.getSession().getAttribute("usuario"));
                if (userMenu != null){
                    iniciadoMenu = true;
                    fechasCalendar=persistenciaMenu.getFechasAsociadas(userMenu.getMail());
                    if (fechasCalendar == null){
                        tamanyo=0;
                    } else {
                        tamanyo = fechasCalendar.size();

                    }
                    //tamanyo = fechasCalendar.size();
                    //long fechaSesion = session.getCreationTime();
                }else {
                response.sendRedirect("/logout");
                }
            }
        } else {
            response.sendRedirect("/logout");
        }
        
%>

<head>
<script src="//code.jquery.com/jquery-1.10.2.js"></script>
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<link rel="stylesheet" type="text/css" href="/css/jquery-ui.css" media="screen, tv, projection" />
<script>
        var tamano=parseInt("<%=tamanyo%>");
        $(function () {
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado'],
                dayNamesShort: ['Dom','Lun','Mar','Mi�','Juv','Vie','S�b'],
                dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','S�'],
                weekHeader: 'Sm',
                dateFormat: 'mm/dd/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            
            $.datepicker.setDefaults($.datepicker.regional['es']);
            $("#datepicker").datepicker({
                minDate: "0D",
                onSelect: function (date) {
                    if (new Date(date).getDate() < 10) {
                        //alert("Fecha: "+new Date(date).getFullYear()+"-"+(new Date(date).getMonth()+1)+"-0"+new Date(date).getDate());
                        if ((new Date(date).getMonth()+1) < 10) {
                            window.location.href = "/citas/viewfecha.jsp?fecha="+new Date(date).getFullYear()+"-0"+(new Date(date).getMonth()+1)+"-0"+new Date(date).getDate();    
                        } else {
                            window.location.href = "/citas/viewfecha.jsp?fecha="+new Date(date).getFullYear()+"-"+(new Date(date).getMonth()+1)+"-0"+new Date(date).getDate();    
                        }
                    } else {
                        if ((new Date(date).getMonth()+1) < 10) {
                            window.location.href = "/citas/viewfecha.jsp?fecha="+new Date(date).getFullYear()+"-0"+(new Date(date).getMonth()+1)+"-"+new Date(date).getDate();
                        } else {
                            window.location.href = "/citas/viewfecha.jsp?fecha="+new Date(date).getFullYear()+"-"+(new Date(date).getMonth()+1)+"-"+new Date(date).getDate();
                        }
                        //alert("Fecha: "+new Date(date).getFullYear()+"-"+(new Date(date).getMonth()+1)+"-"+new Date(date).getDate());           
                    }
                },
                beforeShowDay: shadowDays
            });
        });
        var SelectedDates=[tamano];
        var y = 0;
        if (tamano>0){
            <% SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy HH:mm");  
            for(ii=0; ii<fechasCalendar.size(); ii++) {%>
                 SelectedDates[<%=ii%>]=new Date('<%=formatter.format(fechasCalendar.get(ii))%>');  
            <%} %>
        } else {
            //alert("No hay citas registradas en el sistema");     
        }
        function shadowDays (date) {    
            var day = date.getDay();
            var x=0;
            var high=false;
            var cont=0;
            //var Highlight = SelectedDates[date];
            if (day == 0 ) {
                return [false, ''];
            } else {
                if (tamano>0){
                    while ((x < SelectedDates.length) && (!high)){
                    // alert(SelectedDates[x].getTime());
                    high=new Date (SelectedDates[x].getFullYear(),SelectedDates[x].getMonth(),SelectedDates[x].getDate()).getTime() == new Date(date).getTime();
                    x++;
                    }
                } 
                if (high && cont<3) {
                    cont++;
                    return [true, "Highlighted1 "];
                } else  if (high && cont>=3) {
                    return [true, "Highlighted2 "];
                } else {
                    return [true, "NormalDay "];
                }                              
            }
        };                           
</script>
</head>
<%-- Contenido menu izquierdo --%>
<div id="contentLeft">
    <p>
        <span class="header" >Men&uacute;</span>
    </p>

    <p>
        <a href="/index.jsp" class="menuItem">Inicio</a>
        <a href="/citas/citas.jsp" class="menuItem">Gesti�n de citas</a>
        <% if (iniciadoMenu == true){ %>
        <a href="/admin/administration/user_administration.jsp" class="menuItem">Preferencias</a>
        <a href="/citas/huecos_administration.jsp" class="menuItem">Buscar disponibilidad</a>
        <a href="/logout" class="menuItem">Cerrar sesi&oacute;n</a>
        <% } else{ %>
        <a href="/login.jsp" class="menuItem">Iniciar sesi&oacute;n</a>
        <% } %>
    </p>
    <% if (iniciadoMenu == true){ %> 
        <!-- Menu Izquiero : calendario -->
        <!-- <div class="calendar">--> <div id="datepicker" class="iconMenu"></div>
        <%//@include file="/WEB-INF/include/calendario.jsp" %>
        <!-- </div> --> 
    <% } %>
        <!-- Esquina redondeada en la parte de abajo del menu -->
        <div class="bottomCorner">
            <img src="/images/template/corner_sub_br.gif" alt="bottom corner" class="vBottom"/>
        </div>
</div>
    
  <%-- Fin menu izquierdo --%>