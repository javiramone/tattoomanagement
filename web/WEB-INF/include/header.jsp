<div id="header">
    <!--Esquina redondeada de arriba-->
    <img src="/images/template/corner_tl.gif" alt="corner" style="float:left;" /> 
    <!-- Site title and subTitle -->
    <span class="title">
        <span class="white" >Gesti�n de Citas</span>
        <span class="subTitle">
          &copy; JaviRamone
        </span>
    </span>

    <% Boolean auth = (Boolean) session.getAttribute("auth"); %>
    <!-- El menu se define en orden inverso a como se muestra (caused by float: right) -->
    <a href="/about.jsp" <%= menuAbout %> class="lastMenuItem">Acerca de<span class="desc">Contacto</span></a>

    <% if (auth != null && auth == true){ %>
    <a href="/admin/administration/user_administration.jsp" <%= menuPreferencias %> >Preferencias<span class="desc">gestionar</span></a>
    <% } %>

    <% if (auth == null || auth == false){ %>
        <a href="/login.jsp" <%= menuLogin %> >Login / Registro<span class="desc">iniciar</span></a>
    <% }else { %>
    <a href="/logout" <%= menuLogin %> >Cerrar sesi&oacute;n<span class="desc">salir</span></a>
    <% } %>
    
    <a href="/citas/citas.jsp" <%= menuCitas %> >Citas<span class="desc">Citas</span></a>
    
    <a href="/index.jsp" <%= menuInicio %> >Inicio<span class="desc">Bienvenido</span></a>

</div>