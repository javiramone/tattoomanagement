<%@page import="persistencia.PersistenceInterface"%>
<%@page import="modelo.Usuario"%>
<div id="contentLeft">
    <p>
        <span class="header">Panel de usuario</span> <br />
    </p>
    <% Usuario actualUser = ((PersistenceInterface)application.getAttribute("persistence")).getUser((String)session.getAttribute("usuario")); %>
    <p>
        <span class="subHeader">Opciones</span>
        <a href="/index.jsp" class="menuItem">Inicio</a>  
        <a href="/admin/administration/user_administration.jsp" title="Preferencias" class="menuItem">Preferencias</a>
        <a href="/logout" title="Cerrar sesi&uacute;n" class="menuItem">Cerrar sesi&oacute;n</a>
    </p>
    <!-- Esquina redondeada en la parte de abajo del menu -->
    <div class="bottomCorner">
        <img src="/images/template/corner_sub_br.gif" alt="bottom corner" class="vBottom"/>
    </div>

</div>
  