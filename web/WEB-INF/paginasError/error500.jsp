<%@ page contentType="text/html; charset=UTF-8" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="icon" href="images/icons/icon.png"> </link>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Error Interno</title>
<link rel="stylesheet" type="text/css" href="/css/screen_yellow.css" media="screen, tv, projection" />
</head>

<body>
<!-- Contenedor principal-->
<div id="siteBox">

	<!--Cabecera-->
        <%@include file="/WEB-INF/include/header.jsp" %>


  <!-- Contenido de la pagina -->
  <div id="content">
    <div id="contentLeft">
        <!-- Menu Izquierdo : side bar links/news/search/etc. -->
        <%//@include file="../include/menu.jsp" %>
        
    </div>

    <!-- Contenido de la columna derecha -->
    <div id="contentRight">
        <p>
            <span class="header">Error 500. Error interno</span>
            <img src="/images/icons/error.png" alt="error" align="left" />
            <br />
            <br />
            Ha ocurrido un error interno en el servidor, disculpe las molestias
            <br />
            <br />
            <a href="/index.jsp">Volver a la p&aacute;gina de inicio</a>
            <br /><br /><br /><br /><br /><br /><br />
        </p>

      <!-- Crea las esquinas redondeadas abajo -->
      <img src="/images/template/corner_sub_bl.gif" alt="bottom corner" class="vBottom"/>

    </div>
</div>

<!-- Pie de pagina -->
<%@include file="/WEB-INF/include/footer.jsp" %>
</div>

</body>
</html>

<%! String menuInicio = ""; %>
<%! String menuCitas = ""; %>
<%! String menuLogin = ""; %>
<%! String menuPreferencias = ""; %>
<%! String menuAbout = ""; %>